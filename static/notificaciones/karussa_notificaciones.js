/**
 * Created by metallica on 10/12/15.
 */

swampdragon.ready(function () {
    console.log("FUNCION READY SWAMPDRAGON-KARUSSA");
    swampdragon.subscribe('karussa_notificaciones', 'notificaciones');
});


window.addEventListener('load', function () {
    Notification.requestPermission( function (status)
    {
        console.log("addEventListener -karussa");
        console.log(status);
        console.log(Notification.permission);
        // This allows to use Notification.permission with Chrome/Safari
        if (Notification.permission !== status) {
            console.log(status + " dentro del if -karussa");
            Notification.permission = status;
        }
        if (Notification.permission == 'denied')
        {
            console.log("Usuario denego el permiso de notificaciones ");
            Notification.requestPermission(function (status)
            {
                console.log("segunda peticion de permisos");
                Notification.permission = 'granted';
                console.log(status);
                console.log(Notification.permission);
                console.log("hardcodeando permisos");
            });
        }
    });
});


// Esta a la escucha de cada mensaje enviado desde el router a la vista
swampdragon.onChannelMessage(function (channels, message) {
    console.log("onChanelMessage-karussa");
    console.log(channels);
    console.log(message);
    console.log(message.data);

    console.log(message.data['mensaje']);

    if (message.action === "created") {
        //addNotification((message.data));
        notificacion_video(message.data);
    }
    if (message.action === "updated") {
        console.log("actualizacion updated");
        //addNotification((message.data));
        notificacion_video(message.data);
    }
});


function nueva_notificacion(mensaje) {
    // Si el usuario autorizo mostrar las notificaciones, se crea la notificacion con el mensaje
    if (window.Notification && Notification.permission === "granted") {
        console.log('Tiene permisos para notificaciones');
        randomNotification(mensaje);
    }
}

// Add new notifications
function addNotification(notificacion) {
    // Si el usuario autorizo mostrar las notificaciones, se crea la notificacion con el mensaje
    if (window.Notification && Notification.permission === "granted") {
        console.log('Tiene permisos para notificaciones');
        new Notification(notificacion.mensaje);

    }
}

function notificacion_video(notificacion) {
    // Si el usuario autorizo mostrar las notificaciones, se crea la notificacion con el mensaje
    if (window.Notification && Notification.permission === "granted") {
        console.log('Tiene permisos para notificaciones');
        //new Notification(notificacion.mensaje);
        window.setTimeout(function () {
            var instance = new Notification
            (
                notificacion.mensaje, {
                    body: "ir a ver renka!",
                    icon: "/static/base/img/frida_30x30.jpg"
                }
            );
            instance.onclick = function () {
                console.log('click');
                window.location = 'http://192.168.0.105' + notificacion.url_video; //URL
                instance.close()
            };
            instance.onerror = function () {
                console.log('algo raro paso D\':')
            };
            instance.onshow = function () {
                console.log('si llego')
            };
            instance.onclose = function (x) {
                console.log(x);
                if (x == 1) {
                    console.log('se salio por tiempo')
                } else {
                    console.log('se salio por gusto')
                }
                window.location = notificacion.url_video; //URL
            };
            setTimeout(function () {
                console.log('se acabo el tiempo');
                instance.onclose(1)
            }, 3000)
        }, 1000);
    };
};

function spawnNotification(theBody, theIcon, theTitle) {
    var options = {
        body: theBody,
        icon: theIcon
    };
    var n = new Notification(theTitle, options);
    setTimeout(n.close.bind(n), 5000);
}
