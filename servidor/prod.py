#!/usr/bin/env python
__author__ = 'metallica'

import os
import sys
from swampdragon.swampdragon_server import run_server

#especificamos con que configuracion de proyecto vamos a correr el servidor de redis
#   CONFIGURACION PRODUCCION
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "karussa.settings.prod")
host_port = sys.argv[1] if len(sys.argv) > 1 else None
#corremos el servidor
run_server(host_port=host_port)

