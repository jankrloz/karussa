from swampdragon import route_handler
from swampdragon.route_handler import ModelPubRouter, BaseRouter, ModelRouter
from .models import Notification
from .serializers import NotificationSerializer

'''
class NotificationRouter(ModelPubRouter):
    valid_verbs = ['subscribe']
    route_name = 'notifications'
    model = Notification
    serializer_class = NotificationSerializer

class NotificationRouter(BaseRouter):
    valid_verbs = ['subscribe']
    route_name = 'notifications'
    model = Notification
    serializer_class = NotificationSerializer

    def get_subscription_channels(**kwargs):
        logger.info("GET SUBSCRIPTION_CHANNELS ")
        logger.info(kwargs)
        if 'group_a' in kwargs:
            return ['group_a']
        if 'group_b' in kwargs:
            return ['group_b']
        return ['group_a', 'group_b']
'''


class NotificationRouter(ModelRouter):

    route_name = 'notifications'
    model = Notification
    serializer_class = NotificationSerializer

    def get_object(self, **kwargs):
        logger.info("GET_OBJECT")
        logger.info(vars(self))
        logger.info(kwargs)
        return self.model.objects.get(pk=kwargs['pk'])

    def get_query_set(self, **kwargs):
        logger.info("GET_QUERY_SET")
        logger.info(vars(self))
        logger.info(kwargs)
        return self.model.objects.all()



route_handler.register(NotificationRouter)





