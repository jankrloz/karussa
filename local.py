#!/usr/bin/env python
__author__ = 'metallica'

import os
import sys
from swampdragon.swampdragon_server import run_server

#especificamos con que configuracion de proyecto vamos a correr el servidor de redis
#   CONFIGURACION LOCAL
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "karussa.settings.local")
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "karussa.settings.local")
host_port = sys.argv[1] if len(sys.argv) > 1 else None
#corremos el servidor
run_server(host_port=host_port)

