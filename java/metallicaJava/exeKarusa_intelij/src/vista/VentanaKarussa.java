/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.JFXPanel;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author metallica
 */
public class VentanaKarussa extends JFrame
{
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenuItem maximizar;
    private javax.swing.JMenuItem salir;
    private final JFXPanel jfxPanel = new JFXPanel();
    private final JPanel panel = new JPanel(new BorderLayout());

    public VentanaKarussa() 
        {
            super();
            initComponents();
        }
    private void initComponents() {
        jPopupMenu1 = new javax.swing.JPopupMenu();
        maximizar = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        salir = new javax.swing.JMenuItem();
        JPanel topBar = new JPanel(new BorderLayout(5, 0));

  //      maximizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/home/metallica/Escritorio/karussa/static/base/img/frida_30x30.jpg"))); // NOI18N
        maximizar.setText("jMenuItem1");
        
        /// inicializar en Stage_karusa
        //maximizar.addActionListener(new java.awt.event.ActionListener() {
        //    public void actionPerformed(java.awt.event.ActionEvent evt) {
        //        maximizarActionPerformed(evt);
        //    }
        //});
        //
        jPopupMenu1.add(maximizar);
        jPopupMenu1.add(jSeparator1);

//        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/home/metallica/Escritorio/karussa/static/base/img/frida_30x30.jpg"))); // NOI18N
        salir.setText("jMenuItem2");
        /// inicializar en Stage_karusa
        //salir.addActionListener(new java.awt.event.ActionListener() {
        //    public void actionPerformed(java.awt.event.ActionEvent evt) {
        //        salirActionPerformed(evt);
        //    }
        //});
        ///

        jPopupMenu1.add(salir);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(1024, 600));

        /// inicializar en Stage_karusa
        //addWindowListener(new java.awt.event.WindowAdapter() {
        //    public void windowClosed(java.awt.event.WindowEvent evt) {
        //        formWindowClosed(evt);
        //    }
        //});
        //

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 403, Short.MAX_VALUE)
        );

        
        panel.add(topBar, BorderLayout.NORTH);
        panel.add(jfxPanel, BorderLayout.CENTER);
        //panel.add(statusBar, BorderLayout.SOUTH);

        getContentPane().add(panel);

        setPreferredSize(new Dimension(1024, 600));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();

    }
    
   public JFXPanel getJFXPanel()
   {
       return this.jfxPanel;
   }
        
}


