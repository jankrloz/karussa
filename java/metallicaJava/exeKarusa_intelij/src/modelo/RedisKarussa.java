package modelo;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import nicon.notify.core.Notification;
import org.json.JSONObject;


public class RedisKarussa extends JedisPool
{    
    List<Thread> tds = new ArrayList<Thread>();
    Jedis jedis = null;
    RedisKarussaPubSub conexionRedis = null;
    
    // Constructor 
    public RedisKarussa(String host, int port)
    {
        super(host, port);
        this.jedis = new Jedis(host);
        this.conexionRedis = new RedisKarussaPubSub(host, port);
    }
    
    public void crear_coneccion()
    {
        
    }
    
    public void subscribirse(String canales)
    {
        System.out.println("jedis : ");
        System.out.println(this.jedis);
        System.out.println("conexxionRedis: ");
        System.out.println(this.conexionRedis);
        jedis.subscribe(this.conexionRedis, canales);
    }
    
    
    public static void main(String args [])
    {
        System.out.println("metodo main RedisKarussa");
        int port = 6379;
        String host = "192.168.0.103";
        String canal = "karussa|usuario__id:3";
        RedisKarussa pool = new RedisKarussa(host, port);
        System.out.println("SE CARGO LA CONEXION A REDIS");
        pool.subscribirse(canal);
        System.out.println("SE CONECTO");
        
    }
    
}
