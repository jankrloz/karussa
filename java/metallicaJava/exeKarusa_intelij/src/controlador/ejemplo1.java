/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

//import redis.clients.jedis.Jedis;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javax.swing.*;
import static javafx.concurrent.Worker.State.FAILED;

public class ejemplo1 extends JFrame {

    private final JFXPanel jfxPanel = new JFXPanel();
    private WebEngine engine;

    private final JPanel panel = new JPanel(new BorderLayout());
    //private final JLabel lblStatus = new JLabel();

    //private final JButton btnGo = new JButton("Go");
    //private final JTextField txtURL = new JTextField();
    //private final JProgressBar progressBar = new JProgressBar();
    public ejemplo1() {
        super();
        initComponents();
        /*
        TrayIcon trayIcon = null;
        if (SystemTray.isSupported()) {
            SystemTray tray = SystemTray.getSystemTray();
            Image image = Toolkit.getDefaultToolkit().getImage("C:\\Users\\USER\\Documents\\NetBeansProjects\\redisCliente\\src\\iconos\\iconito.png");
            ActionListener listener = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                }
            };
            PopupMenu popup = new PopupMenu();
            MenuItem defaultItem = new MenuItem();
            defaultItem.addActionListener(listener);
            popup.add(defaultItem);
            trayIcon = new TrayIcon(image, "Tray Demo", popup);
            trayIcon.addActionListener(listener);
            try {
                tray.add(trayIcon);
            } catch (AWTException e) {
                System.err.println(e);
            }
        }
        */
    }

    private void initComponents() {
        createScene();
        /*
         ActionListener al = new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
         loadURL(txtURL.getText());
         }
         };
         */
        //btnGo.addActionListener(al);
        //txtURL.addActionListener(al);
        //txtURL.setEditable(false);

        //progressBar.setPreferredSize(new Dimension(150, 18));
        //progressBar.setStringPainted(true);
        JPanel topBar = new JPanel(new BorderLayout(5, 0));
        topBar.setBorder(BorderFactory.createEmptyBorder(3, 5, 3, 5));
        //topBar.add(txtURL, BorderLayout.CENTER);
        //topBar.add(btnGo, BorderLayout.EAST);

        //JPanel statusBar = new JPanel(new BorderLayout(5, 0));
        //statusBar.setBorder(BorderFactory.createEmptyBorder(3, 5, 3, 5));
        //statusBar.add(lblStatus, BorderLayout.CENTER);
        //statusBar.add(progressBar, BorderLayout.EAST);
        panel.add(topBar, BorderLayout.NORTH);
        panel.add(jfxPanel, BorderLayout.CENTER);
        //panel.add(statusBar, BorderLayout.SOUTH);

        getContentPane().add(panel);

        setPreferredSize(new Dimension(1024, 600));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();

    }

    private void createScene() {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {

                WebView view = new WebView();
                engine = view.getEngine();
                /*
                 engine.titleProperty().addListener(new ChangeListener<String>() {
                 @Override
                 public void changed(ObservableValue<? extends String> observable, String oldValue, final String newValue) {
                 SwingUtilities.invokeLater(new Runnable() {
                 @Override
                 public void run() {
                 ejemplo1.this.setTitle(newValue);
                 }
                 });
                 }
                 });
                 
                 engine.setOnStatusChanged(new EventHandler<WebEvent<String>>() {
                 @Override
                 public void handle(final WebEvent<String> event) {
                 SwingUtilities.invokeLater(new Runnable() {
                 @Override
                 public void run() {
                 lblStatus.setText(event.getData());
                 }
                 });
                 }
                 });
                 */
                engine.locationProperty().addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> ov, String oldValue, final String newValue) {
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                //txtURL.setText(newValue);
                            }
                        });
                    }
                });

                engine.getLoadWorker()
                        .exceptionProperty()
                        .addListener(new ChangeListener<Throwable>() {

                            public void changed(ObservableValue<? extends Throwable> o, Throwable old, final Throwable value) {
                                if (engine.getLoadWorker().getState() == FAILED) {
                                    SwingUtilities.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            JOptionPane.showMessageDialog(
                                                    panel,
                                                    (value != null)
                                                            ? engine.getLocation() + "\n" + value.getMessage()
                                                            : engine.getLocation() + "\nUnexpected error.",
                                                    "Loading error...",
                                                    JOptionPane.ERROR_MESSAGE);
                                        }
                                    });
                                }
                            }
                        });

                jfxPanel.setScene(new Scene(view));
            }
        });
    }

    public void loadURL(final String url) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                String tmp = toURL(url);

                if (tmp == null) {
                    tmp = toURL("http://" + url);
                }

                engine.load(tmp);
            }
        });
    }

    private static String toURL(String str) {
        try {
            return new URL(str).toExternalForm();
        } catch (MalformedURLException exception) {
            return null;
        }
    }

    public static void main(String[] args) {
        //Jedis jedis = new Jedis("192.168.0.11");
        System.out.println("Connection to server sucessfully");
        //check whether server is running or not
        //jedis.set("tutorial-name", "Redis tutorial");
        // Get the stored data and print it

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                ejemplo1 browser = new ejemplo1();
                browser.setVisible(true);
                browser.loadURL("www.google.com.mx");
                //System.out.println("Server is running: " + jedis.ping());
            }
        });
    }
}
