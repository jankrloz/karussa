package exekarussa;
/*
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import vista.Vista_fxmlController;

public class ExeKarussa extends Application {

    public static void main(String[] args) {
        Application.launch(ExeKarussa.class, (java.lang.String[])null);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            System.out.println("INICIAMOS EL PROGRAMA ");
            System.out.println(ExeKarussa.class.getResource("/vista/vista_fxml.fxml"));
            
            Logger.getLogger(ExeKarussa.class.getName()).log(Level.SEVERE, null, ExeKarussa.class.getResource("/vista/vista_fxml.fxml"));
            //StackPane page = (StackPane) FXMLLoader.load(ExeKarussa.class.getResource("/vista/vista_fxml.fxml"));
            AnchorPane page = (AnchorPane) FXMLLoader.load(ExeKarussa.class.getResource("/vista/vista_fxml.fxml"));
            Scene scene = new Scene(page);
            primaryStage.setScene(scene);
            primaryStage.setTitle("FXML is Simple");
            primaryStage.show();
        } catch (Exception ex) {
            Logger.getLogger(ExeKarussa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
*/

import java.awt.TrayIcon;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebEngine;
import javafx.stage.Stage;
import vista.Stage_karussa;

public class ExeKarussa extends Application 
{
    private boolean firstTime;
    private TrayIcon trayIcon;
    private WebEngine engine;

    public static void main(String[] args)
    {
        launch(args);
    }
    @Override
    public void start(Stage stage) throws Exception {
        System.out.println("CREAR STAGE_KARUSSA");
        Stage_karussa stage_karussa = new Stage_karussa();
        
        //createTrayIcon(stage);
        firstTime = true;
        Platform.setImplicitExit(false);
        
        //Scene scene_ = new Scene(new Group(), 800, 600);
        //stage_karussa.setScene(scene_);
        //stage_karussa.show();
        
        
        //*  HACER ALGO USANDO FXML
        System.out.println("Intentando cargar SceneKarussa con Stage_Karussa");
        System.out.println(ExeKarussa.class.getResource("/vista/vista_fxml.fxml"));

        Logger.getLogger(ExeKarussa.class.getName()).log(Level.SEVERE, null, ExeKarussa.class.getResource("/vista/vista_fxml.fxml"));
        //StackPane page = (StackPane) FXMLLoader.load(ExeKarussa.class.getResource("/vista/vista_fxml.fxml"));
        AnchorPane page = (AnchorPane) FXMLLoader.load(ExeKarussa.class.getResource("/vista/vista_fxml.fxml"));

        Scene scene = new Scene(page);
        stage_karussa.setScene(scene);
        stage_karussa.setTitle("FXML is Simple");
        stage_karussa.show();

        System.out.println("Termino y muestra el Stage Principal");

        /*
        System.out.println("CREAR EL VIEW SIN USAR LA APLICACION START");
        JFrameKarussa ventana = new JFrameKarussa();
        //ventana.setVisible(firstTime);
        System.out.println(ventana.isVisible());
        if(!ventana.isVisible())
        {
            ventana.setVisible(true);
        }
        System.out.println(ventana.isVisible());
        
        System.out.println("CREAR CONEXXION CON REDIS");
        int port = 6379;
        String host = "192.168.0.103";
        String canal = "karussa|usuario__id:3";
        RedisKarussa pool = new RedisKarussa(host, port);
        System.out.println("SE CARGO LA CONEXION A REDIS");
        pool.subscribirse(canal);
        System.out.println("SE CONECTO");
        */

    }
}


