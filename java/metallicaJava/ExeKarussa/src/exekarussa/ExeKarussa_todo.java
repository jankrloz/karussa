package exekarussa;

//import redis.clients.jedis.Jedis;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javax.swing.*;
import static javafx.concurrent.Worker.State.FAILED;
import java.awt.SystemTray;

public class ExeKarussa_todo extends JFrame {

    private final JFXPanel jfxPanel = new JFXPanel();
    private WebEngine engine;

    SystemTray st;
    TrayIcon ti;
    Image imagenIco;
    
    private final JPanel panel = new JPanel(new BorderLayout());
    //private final JLabel lblStatus = new JLabel();

    //private final JButton btnGo = new JButton("Go");
    //private final JTextField txtURL = new JTextField();
    //private final JProgressBar progressBar = new JProgressBar();
    public ExeKarussa_todo() {
        super();
        initComponents();
        ///*
        TrayIcon trayIcon = null;
        if (SystemTray.isSupported()) {
            SystemTray tray = SystemTray.getSystemTray();
            Image image = Toolkit.getDefaultToolkit().getImage("/home/metallica/Escritorio/karussa/static/base/img/frida_30x30.jpg");
            ActionListener listener = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                }
            };
            PopupMenu popup = new PopupMenu();
            MenuItem defaultItem = new MenuItem();
            defaultItem.addActionListener(listener);
            popup.add(defaultItem);
            trayIcon = new TrayIcon(image, "Tray Demo", popup);
            trayIcon.addActionListener(listener);
            try {
                tray.add(trayIcon);
            } catch (AWTException e) {
                System.err.println(e);
            }
        }
        //*/
    }

    private void maximizarActionPerformed(java.awt.event.ActionEvent evt) {                                          
        // TODO add your handling code here:
        setVisible(true);
        toFront();
        st.remove(ti);
    }                                         

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {                                      
        // TODO add your handling code here:
        System.exit(0);
    }                                     

    private void formWindowClosed(java.awt.event.WindowEvent evt) {                                  
        // TODO add your handling code here:
        setVisible(false);
        try {
            st.add(ti);
        } catch (AWTException ex) {
            Logger.getLogger(ExeKarussa_todo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }                                 
    
    
    private void initComponents() {
        createScene();

        jPopupMenu1 = new javax.swing.JPopupMenu();
        maximizar = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        salir = new javax.swing.JMenuItem();
        JPanel topBar = new JPanel(new BorderLayout(5, 0));

  //      maximizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/home/metallica/Escritorio/karussa/static/base/img/frida_30x30.jpg"))); // NOI18N
        maximizar.setText("jMenuItem1");
        maximizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maximizarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(maximizar);
        jPopupMenu1.add(jSeparator1);

//        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/home/metallica/Escritorio/karussa/static/base/img/frida_30x30.jpg"))); // NOI18N
        salir.setText("jMenuItem2");
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        jPopupMenu1.add(salir);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(1024, 600));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 403, Short.MAX_VALUE)
        );

        
        panel.add(topBar, BorderLayout.NORTH);
        panel.add(jfxPanel, BorderLayout.CENTER);
        //panel.add(statusBar, BorderLayout.SOUTH);

        getContentPane().add(panel);

        setPreferredSize(new Dimension(1024, 600));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();

    }

    private void createScene() {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {

                WebView view = new WebView();
                engine = view.getEngine();
                /*
                 engine.titleProperty().addListener(new ChangeListener<String>() {
                 @Override
                 public void changed(ObservableValue<? extends String> observable, String oldValue, final String newValue) {
                 SwingUtilities.invokeLater(new Runnable() {
                 @Override
                 public void run() {
                 ejemplo1.this.setTitle(newValue);
                 }
                 });
                 }
                 });
                 
                 engine.setOnStatusChanged(new EventHandler<WebEvent<String>>() {
                 @Override
                 public void handle(final WebEvent<String> event) {
                 SwingUtilities.invokeLater(new Runnable() {
                 @Override
                 public void run() {
                 lblStatus.setText(event.getData());
                 }
                 });
                 }
                 });
                 */
                engine.locationProperty().addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> ov, String oldValue, final String newValue) {
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                //txtURL.setText(newValue);
                            }
                        });
                    }
                });

                engine.getLoadWorker()
                        .exceptionProperty()
                        .addListener(new ChangeListener<Throwable>() {

                            public void changed(ObservableValue<? extends Throwable> o, Throwable old, final Throwable value) {
                                if (engine.getLoadWorker().getState() == FAILED) {
                                    SwingUtilities.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            JOptionPane.showMessageDialog(
                                                    panel,
                                                    (value != null)
                                                            ? engine.getLocation() + "\n" + value.getMessage()
                                                            : engine.getLocation() + "\nUnexpected error.",
                                                    "Loading error...",
                                                    JOptionPane.ERROR_MESSAGE);
                                        }
                                    });
                                }
                            }
                        });

                jfxPanel.setScene(new Scene(view));
            }
        });
    }

    public void loadURL(final String url) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                String tmp = toURL(url);

                if (tmp == null) {
                    tmp = toURL("http://" + url);
                }

                engine.load(tmp);
            }
        });
    }

    private static String toURL(String str) {
        try {
            return new URL(str).toExternalForm();
        } catch (MalformedURLException exception) {
            return null;
        }
    }

    public static void main(String[] args) {
        //Jedis jedis = new Jedis("192.168.0.11");
        System.out.println("Connection to server sucessfully");
        //check whether server is running or not
        //jedis.set("tutorial-name", "Redis tutorial");
        // Get the stored data and print it

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                ExeKarussa_todo browser = new ExeKarussa_todo();
                browser.setVisible(true);
                browser.loadURL("www.google.com.mx");
                //System.out.println("Server is running: " + jedis.ping());
            }
        });
    }
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenuItem maximizar;
    private javax.swing.JMenuItem salir;
    
}
