package modelo;

import java.util.Arrays;
import java.util.Iterator;
import nicon.notify.core.Notification;
import org.json.JSONObject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public class RedisKarussaPubSub extends JedisPubSub
{
    private int port=-1;
    private String host=null;    
    
    public RedisKarussaPubSub(String host, int port)
    {
        super();
    }

    @Override
    public void onMessage(String channel, String message) {
        System.out.println(message);
        String result[] = new String[3];
        recorrer(message, result);
        System.out.println(Arrays.toString(result));
        //JOptionPane.showMessageDialog(null, message, "Mensaje desde el server Redis", JOptionPane.OK_CANCEL_OPTION);
        Notification.showConfirm("titulo", result[0], Notification.NICON_DARK_THEME, true, 100000);

    }

    @Override
    public void onPMessage(String pattern, String channel, String message) {
        System.out.println("Opcion2 de mensajes, " + pattern + " canal " + channel + " mensaje: " + message);
    }

    @Override
    public void onSubscribe(String channel, int subscribedChannels) {
        System.out.println("se unio al canal: " + channel + " " + subscribedChannels);
    }

    @Override
    public void onUnsubscribe(String channel, int subscribedChannels) {
        System.out.println("salio del canal: " + channel + " " + subscribedChannels);
    }

    @Override
    public void onPUnsubscribe(String pattern, int subscribedChannels) {
        System.out.println("hizo algo con : " + pattern + " " + subscribedChannels);
    }

    @Override
    public void onPSubscribe(String pattern, int subscribedChannels) {
        System.out.println("hizo algo con : " + pattern + " " + subscribedChannels);
    }

    /// Leer el mensaje
    private static void recorrer(String raizJson, String[] result) {
        try {
            JSONObject json = new JSONObject(raizJson);
            Iterator<?> nivel = json.keys();
            while (nivel.hasNext()) {
                String value = (String) nivel.next();
                //System.out.println(value);

                int x = json.get(value).toString().lastIndexOf('{');
                int y = json.get(value).toString().lastIndexOf('[');
                if (x > -1) {
                    recorrer(json.get(value).toString(), result);
                } else {
                    switch (value) {
                        case "mensaje":
                            result[0] = value;
                            break;
                        case "url_video":
                            result[1] = value;
                            break;
                        case "imagen":
                            result[2] = value;
                            break;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
            System.out.println(e.getCause());
            System.out.println(e.getClass());
        }
        //return result;
    }
    
    
}
