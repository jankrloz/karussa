package vista;

/**
 *
 * @author metallica
 */
import java.net.MalformedURLException;
import java.net.URL;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import static javafx.concurrent.Worker.State.FAILED;
import javax.swing.text.View;


public class JFXPanelKarussa extends JFXPanel {
    private WebEngine navegador;
    private WebView view;
    private String URL;
    private static JFXPanelKarussa instance = null;
    private JFXPanelKarussa(){}
    

    protected static JFXPanelKarussa getInstance()
    {
        System.out.println("getInstance");
        if (instance == null)
        {
            instance = new JFXPanelKarussa();
            instance.inicializar();
            System.out.println("Se corrio inicializar");
        }
        return instance;
    }
    
    protected static JFXPanelKarussa getInstance(WebView view, WebEngine navegador)
    {
        System.out.println("getInstance");
        if (instance == null)
        {
            instance = new JFXPanelKarussa();
            instance.inicializar();
            //instance.inicializar(view, navegador);
            System.out.println("Se corrio inicializar");
        }
        return instance;
    }
    
    private static void inicializar()
    {
                
        Platform.runLater(
                new Runnable() {
            @Override
            public void run() {
                instance.view =  new WebView();
                instance.navegador = new WebEngine();
                instance.navegador.locationProperty().addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> ov, String oldValue, final String newValue) {
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                });
                instance.getLoadWorkerNavegador();
                instance.setScene(new Scene(instance.view));        
            }
        });        
    }
    
    private static void getLoadWorkerNavegador()
    {
        instance.navegador.getLoadWorker().exceptionProperty()
                .addListener(new ChangeListener<Throwable>() {
                    public void changed(ObservableValue<? extends Throwable> o, Throwable old, final Throwable value) {
                        if (instance.navegador.getLoadWorker().getState() == FAILED) {
                            SwingUtilities.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    JOptionPane.showMessageDialog(
                                            null,
                                            (value != null)
                                                    ? instance.navegador.getLocation() + "\n" + value.getMessage()
                                                    : instance.navegador.getLocation() + "\nUnexpected error.",
                                            "Loading error...",
                                            JOptionPane.ERROR_MESSAGE);
                                }
                            });
                        }
                    }
                });                
    }
    
    public void loadURL(final String url) 
    {
        Platform.runLater(new Runnable() 
        {
            @Override
            public void run() 
            {
                System.out.println("loadURL");
                System.out.println(instance.URL);
                System.out.println(instance.navegador);
                System.out.println(instance.view);
                System.out.println();
                System.out.println("entrando a cargar url");
                String tmp = toURL(url);
                System.out.println(tmp);
                if (tmp == null) {
                    tmp = toURL("http://" + url);
                }
                instance.navegador.load(tmp);
            }
        });
    }
    
    protected static String toURL(String str) 
    {
        try 
        {
            return new URL(str).toExternalForm();
        } 
        catch (MalformedURLException exception) 
        {
            System.out.println(exception);
            return null;
        }
    }
    protected static void setURL (String url)
    {
        instance.URL = url;
    }
    protected WebEngine getNavegador()
    {
        return instance.navegador;
    }
    
    
}
