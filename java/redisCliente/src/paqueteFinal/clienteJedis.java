package paqueteFinal;

import java.awt.Desktop;
import java.awt.HeadlessException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import javax.swing.JOptionPane;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;

public class clienteJedis {

    JedisPool jp;
    Jedis cliente;
    String canales[];
    JedisPubSub jsp;
    int estado;

    public clienteJedis(String ip, String canal) {
        this.jp = new JedisPool(ip);
        this.cliente = new Jedis(ip);
        this.canales = new String[1];
        canales[0] = canal;
        new Thread(new Runnable() {
            @Override
            public void run() {
                jsp = new JedisPubSub() {
                    @Override
                    public void onMessage(String channel, String message) {
                        String result[] = new String[3];
                        result = jsonToString.recorrer(message, result);
                        estado = JOptionPane.showConfirmDialog(null, Arrays.toString(result));
                        notificacionCiclada(result, estado);
                    }

                    @Override
                    public void onPMessage(String pattern, String channel, String message) {
                        System.out.println("Opcion2 de mensajes, " + pattern + " canal " + channel + " mensaje: " + message);
                    }

                    @Override
                    public void onSubscribe(String channel, int subscribedChannels) {
                        System.out.println("se unio al canal: " + channel + " " + subscribedChannels);
                    }

                    @Override
                    public void onUnsubscribe(String channel, int subscribedChannels) {
                        System.out.println("salio del canal: " + channel + " " + subscribedChannels);
                    }

                    @Override
                    public void onPUnsubscribe(String pattern, int subscribedChannels) {
                        System.out.println("hizo algo con : " + pattern + " " + subscribedChannels);
                    }

                    @Override
                    public void onPSubscribe(String pattern, int subscribedChannels) {
                        System.out.println("hizo algo con : " + pattern + " " + subscribedChannels);
                    }
                };
                System.out.println("conectado");
                cliente.subscribe(jsp, canales);
            }
        }, "sinNombre").start();

    }

    private void notificacionCiclada(String[] result, int estado) {
        if (estado == 0) {
            try {
                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().browse(new URI("http://" + result[0]));
                } else {
                    JOptionPane.showMessageDialog(null, "valla a la url siguiente :" + result[0]);
                }
                this.estado = -2;
            } catch (URISyntaxException | IOException | HeadlessException e) {
                System.out.println(e.getCause());
                System.out.println(e.getClass());
                System.out.println(e.getLocalizedMessage());

            }
        }
    }

}
