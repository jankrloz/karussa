package paqueteFinal;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class serializador {

    FileOutputStream fos = null;
    ObjectOutputStream salida = null;
    FileInputStream fis = null;
    ObjectInputStream entrada = null;
    static objetoSerializable p;

    public void serializar(String user, String pass) {
        try {
            RandomAccessFile archivo = new RandomAccessFile(new File("./setings.bin"), "rw");
            archivo.writeBytes("");
            archivo.close();
            fos = new FileOutputStream("./setings.bin");
            salida = new ObjectOutputStream(fos);
            p = new objetoSerializable(user, pass);
            salida.writeObject(p);
        } catch (FileNotFoundException e) {
            System.out.println("1" + e.getMessage());
        } catch (IOException e) {
            System.out.println("2" + e.getMessage());
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
                if (salida != null) {
                    salida.close();
                }
            } catch (IOException e) {
                System.out.println("3" + e.getMessage());
            }
        }

    }

    public String[] leerArchivo() {
        String[] datos = new String[2];
        try {
            fis = new FileInputStream("./setings.bin");
            entrada = new ObjectInputStream(fis);
            p = (objetoSerializable) entrada.readObject(); //es necesario el casting
            datos[0] = p.getUsuario();
            datos[1] = p.getPassword();
        } catch (IOException | ClassNotFoundException e) {
            return datos;
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                if (entrada != null) {
                    entrada.close();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        return datos;
    }
}
