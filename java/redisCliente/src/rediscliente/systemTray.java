/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rediscliente;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import nicon.notify.core.Notification;
import org.json.JSONObject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javax.swing.*;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;
import static javafx.concurrent.Worker.State.FAILED;

/**
 *
 * @author USER
 */
public class systemTray extends JFrame {

    private final JFXPanel jfxPanel = new JFXPanel();
    private WebEngine engine;
    private JPanel panel;

    private static void iniciar_coneccion_redis(String ip, String canal) {
        JedisPool pool = new JedisPool(ip);
        List<Thread> tds = new ArrayList<Thread>();
        Jedis jedis = new Jedis(ip);
        String canales[] = new String[1];
        canales[0] = canal;
        JedisPubSub jps = new JedisPubSub() {
            @Override
            public void onMessage(String channel, String message) {
                System.out.println(message);
                String result[] = new String[3];
                recorrer(message, result);
                System.out.println(Arrays.toString(result));
                //JOptionPane.showMessageDialog(null, message, "Mensaje desde el server Redis", JOptionPane.OK_CANCEL_OPTION);
                Notification.showConfirm("titulo", result[0], Notification.NICON_DARK_THEME, true, 100000);

            }

            @Override
            public void onPMessage(String pattern, String channel, String message) {
                System.out.println("Opcion2 de mensajes, " + pattern + " canal " + channel + " mensaje: " + message);
            }

            @Override
            public void onSubscribe(String channel, int subscribedChannels) {
                System.out.println("se unio al canal: " + channel + " " + subscribedChannels);
            }

            @Override
            public void onUnsubscribe(String channel, int subscribedChannels) {
                System.out.println("salio del canal: " + channel + " " + subscribedChannels);
            }

            @Override
            public void onPUnsubscribe(String pattern, int subscribedChannels) {
                System.out.println("hizo algo con : " + pattern + " " + subscribedChannels);
            }

            @Override
            public void onPSubscribe(String pattern, int subscribedChannels) {
                System.out.println("hizo algo con : " + pattern + " " + subscribedChannels);
            }
        };
        jedis.subscribe(jps, canales);
    }

    private static void recorrer(String raizJson, String[] result) {
        try {
            JSONObject json = new JSONObject(raizJson);
            Iterator<?> nivel = json.keys();
            while (nivel.hasNext()) {
                String value = (String) nivel.next();
                //System.out.println(value);

                int x = json.get(value).toString().lastIndexOf('{');
                int y = json.get(value).toString().lastIndexOf('[');
                if (x > -1) {
                    recorrer(json.get(value).toString(), result);
                } else {
                    switch (value) {
                        case "mensaje":
                            result[0] = value;
                            break;
                        case "url_video":
                            result[1] = value;
                            break;
                        case "imagen":
                            result[2] = value;
                            break;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
            System.out.println(e.getCause());
            System.out.println(e.getClass());
        }
        //return result;
    }
    SystemTray st;
    TrayIcon ti;
    Image imagenIco;

    /**
     * Creates new form systemTray
     */
    public systemTray() {

        initComponents();
        initComponents2();
        if (SystemTray.isSupported()) {
            st = SystemTray.getSystemTray();
            imagenIco = new ImageIcon(getClass().getResource("/iconos/iconito.png")).getImage();

            ti = new TrayIcon(imagenIco, "SystemTray", null);
            maximizar.setText("Maximizar");
            salir.setText("Salir");
            ti.addMouseListener(new MouseAdapter() {
                public void mouseReleased(MouseEvent evt) {
                    jPopupMenu1.setLocation(evt.getX(), evt.getY());
                    jPopupMenu1.setInvoker(jPopupMenu1);
                    jPopupMenu1.setVisible(true);
                }
            });
            ti.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    ti.displayMessage("MenuItem Maximizar", "MenuItem2 Salir", TrayIcon.MessageType.INFO);
                }
            });
            ti.setImageAutoSize(true);
        } else {
            JOptionPane.showMessageDialog(this, "no furula minimizar 2", "no furula minimizar 1", JOptionPane.OK_OPTION);
        }
        setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        maximizar = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        salir = new javax.swing.JMenuItem();

        maximizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/ic_open_in_new_black_24dp_1x.png"))); // NOI18N
        maximizar.setText("jMenuItem1");
        maximizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maximizarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(maximizar);
        jPopupMenu1.add(jSeparator1);

        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/ic_exit_to_app_black_24dp_1x.png"))); // NOI18N
        salir.setText("jMenuItem2");
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        jPopupMenu1.add(salir);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(1024, 600));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 500, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 403, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void initComponents2() {
        createScene();
        panel = new JPanel(new BorderLayout());
        JPanel topBar = new JPanel(new BorderLayout(5, 0));
        topBar.setBorder(BorderFactory.createEmptyBorder(3, 5, 3, 5));
        panel.add(topBar, BorderLayout.NORTH);
        panel.add(jfxPanel, BorderLayout.CENTER);
        this.getContentPane().add(panel);
    }

    private void createScene() {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {

                WebView view = new WebView();
                engine = view.getEngine();
                engine.locationProperty().addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> ov, String oldValue, final String newValue) {
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                });

                engine.getLoadWorker()
                        .exceptionProperty()
                        .addListener(new ChangeListener<Throwable>() {
                            public void changed(ObservableValue<? extends Throwable> o, Throwable old, final Throwable value) {
                                if (engine.getLoadWorker().getState() == FAILED) {
                                    SwingUtilities.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            JOptionPane.showMessageDialog(
                                                    panel,
                                                    (value != null)
                                                            ? engine.getLocation() + "\n" + value.getMessage()
                                                            : engine.getLocation() + "\nUnexpected error.",
                                                    "Loading error...",
                                                    JOptionPane.ERROR_MESSAGE);
                                        }
                                    });
                                }
                            }
                        });

                jfxPanel.setScene(new Scene(view));
            }
        });
    }

    public void loadURL(final String url) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                String tmp = toURL(url);
                System.out.println(tmp);
                if (tmp == null) {
                    tmp = toURL("http://" + url);
                }
                engine.load(tmp);
            }
        });
    }

    private static String toURL(String str) {
        try {
            String url2 = new URL(str).toExternalForm();
            System.out.println(url2);
            return url2;
        } catch (MalformedURLException exception) {
            System.out.println(exception.getCause());
            System.out.println(exception.getClass());
            return null;
        }
    }
    private void maximizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maximizarActionPerformed
        setVisible(true);
        toFront();
        st.remove(ti);
    }//GEN-LAST:event_maximizarActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_salirActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        setVisible(false);
        try {
            st.add(ti);
        } catch (AWTException ex) {
            Logger.getLogger(systemTray.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                systemTray browser = new systemTray();
                browser.setVisible(true);
                browser.loadURL("http://www.google.com.mx");
                System.out.println("se cargo la url");
            }
        });
        try {
            iniciar_coneccion_redis("192.168.0.103", "karussa|usuario__id:3");
        } catch (Exception e) {
            System.out.println(e.getCause());
            System.out.println(e.getClass());
        }
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(systemTray.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(systemTray.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(systemTray.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(systemTray.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenuItem maximizar;
    private javax.swing.JMenuItem salir;
    // End of variables declaration//GEN-END:variables
}
