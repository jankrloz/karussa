/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rediscliente;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;
import nicon.notify.core.Notification;
import org.json.JSONObject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;

/**
 *
 * @author rkrd_
 */
public class clienteJedis {

    JedisPool jp;
    List<Thread> hilos;
    Jedis cliente;
    String canales[];
    JedisPubSub jsp;
    int estado = -1;

    public clienteJedis(String ip, String canal) {
        this.jp = new JedisPool(ip);
        this.hilos = new ArrayList<Thread>();
        this.cliente = new Jedis(ip);
        this.canales = new String[1];
        canales[0] = canal;

        this.jsp = new JedisPubSub() {
            @Override
            public void onMessage(String channel, String message) {
                System.out.println(message);
                String result[] = new String[3];
                recorrer(message, result);
                System.out.println(Arrays.toString(result));
                //JOptionPane.showMessageDialog(null, message, "Mensaje desde el server Redis", JOptionPane.OK_CANCEL_OPTION);
                System.out.println("reultado notificacion");
                estado = JOptionPane.showConfirmDialog(null, message, message, 0, 0);
                System.out.println(JOptionPane.showConfirmDialog(null, "mensaje"));
                //System.out.println(Notification.showConfirm("titulo", "mensaje", Notification.NICON_LIGHT_THEME));
                //System.out.println(Notification.showConfirm("titulo", result[0], Notification.NICON_DARK_THEME, true, 100000));
                System.out.println("fin de la rspuesta");

            }

            @Override
            public void onPMessage(String pattern, String channel, String message) {
                System.out.println("Opcion2 de mensajes, " + pattern + " canal " + channel + " mensaje: " + message);
            }

            @Override
            public void onSubscribe(String channel, int subscribedChannels) {
                System.out.println("se unio al canal: " + channel + " " + subscribedChannels);
            }

            @Override
            public void onUnsubscribe(String channel, int subscribedChannels) {
                System.out.println("salio del canal: " + channel + " " + subscribedChannels);
            }

            @Override
            public void onPUnsubscribe(String pattern, int subscribedChannels) {
                System.out.println("hizo algo con : " + pattern + " " + subscribedChannels);
            }

            @Override
            public void onPSubscribe(String pattern, int subscribedChannels) {
                System.out.println("hizo algo con : " + pattern + " " + subscribedChannels);
            }
        };
        this.cliente.subscribe(this.jsp, this.canales);
    }

    private static void recorrer(String raizJson, String[] result) {
        try {
            JSONObject json = new JSONObject(raizJson);
            Iterator<?> nivel = json.keys();
            while (nivel.hasNext()) {
                String value = (String) nivel.next();
                //System.out.println(value);

                int x = json.get(value).toString().lastIndexOf('{');
                int y = json.get(value).toString().lastIndexOf('[');
                if (x > -1) {
                    recorrer(json.get(value).toString(), result);
                } else {
                    switch (value) {
                        case "mensaje":
                            result[0] = value;
                            break;
                        case "url_video":
                            result[1] = value;
                            break;
                        case "imagen":
                            result[2] = value;
                            break;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
            System.out.println(e.getCause());
            System.out.println(e.getClass());
        }
        //return result;
    }
}
