/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rediscliente;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.swing.JOptionPane;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;

/**
 *
 * @author USER
 */
public class RedisCliente {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JedisPool pool = new JedisPool("127.0.0.1");
        List<Thread> tds = new ArrayList<Thread>();
        //Connecting to Redis server on localhost
        Jedis jedis = new Jedis("127.0.0.1");
        System.out.println("Connection to server sucessfully");
        //check whether server is running or not
        System.out.println("Server is running: " + jedis.ping());
        System.out.println("Server is running: " + jedis.info());
        //jedis.notifyAll();
        String canales[] = new String[1];
        canales[0] = "notification";
        JedisPubSub jps = new JedisPubSub() {
            @Override
            public void onMessage(String channel, String message) {
                String contenido = message;
                String[] cuerpo;
                cuerpo = contenido.split(" ");
                JOptionPane.showMessageDialog(null, message, "Mensaje desde el server Redis", JOptionPane.OK_CANCEL_OPTION);
            }

            @Override
            public void onPMessage(String pattern, String channel, String message) {
                System.out.println("Opcion2 de mensajes, " + pattern + " canal " + channel + " mensaje: " + message);
            }

            @Override
            public void onSubscribe(String channel, int subscribedChannels) {
                System.out.println("se unio al canal: " + channel + " " + subscribedChannels);
            }

            @Override
            public void onUnsubscribe(String channel, int subscribedChannels) {
                System.out.println("salio del canal: " + channel + " " + subscribedChannels);
            }

            @Override
            public void onPUnsubscribe(String pattern, int subscribedChannels) {
                System.out.println("hizo algo con : " + pattern + " " + subscribedChannels);
            }

            @Override
            public void onPSubscribe(String pattern, int subscribedChannels) {
                System.out.println("hizo algo con : " + pattern + " " + subscribedChannels);
            }
        };
        jedis.publish("notifications", "plop");
        jedis.subscribe(jps, canales);

    }

}
