/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rediscliente;

import java.net.MalformedURLException;
import java.net.URL;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import static javafx.concurrent.Worker.State.FAILED;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 *
 * @author rkrd_
 */
public class panelNavegador extends JFXPanel {

    WebEngine navegador;
    WebView view;
    String URL;

    public panelNavegador(String newURL) {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                view = new WebView();
                navegador = view.getEngine();
                navegador.getLoadWorker()
                        .exceptionProperty()
                        .addListener(new ChangeListener<Throwable>() {
                            public void changed(ObservableValue<? extends Throwable> o, Throwable old, final Throwable value) {
                                if (navegador.getLoadWorker().getState() == FAILED) {
                                    SwingUtilities.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            JOptionPane.showMessageDialog(
                                                    null,
                                                    (value != null)
                                                            ? navegador.getLocation() + "\n" + value.getMessage()
                                                            : navegador.getLocation() + "\nUnexpected error.",
                                                    "Loading error...",
                                                    JOptionPane.ERROR_MESSAGE);
                                        }
                                    });
                                }
                            }
                        });
                setScene(new Scene(view));
            }
        });
    }

    public void loadURL(final String url) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                System.out.println("entrando a cargar url");
                String tmp = toURL(url);
                System.out.println(tmp);
                if (tmp == null) {
                    tmp = toURL("http://" + url);
                }
                navegador.load(tmp);
            }
        });
    }

    private static String toURL(String str) {
        try {
            return new URL(str).toExternalForm();
        } catch (MalformedURLException exception) {
            System.out.println(exception);
            return null;
        }
    }

    public panelNavegador() {
    }

    public panelNavegador(WebEngine navegador, WebView view, String URL) {
        this.view = view;
        this.navegador = navegador;

        this.URL = URL;
    }

}
