from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect

from apps.logger.funciones import logger
from apps.usuarios.models import RegistroEntrada


def logOut(request):
    try:
        if request.user.is_authenticated():
            usuario = request.user
            registros = RegistroEntrada.objects.filter(usuario=usuario, salio=False)
            if registros.exists():
                logger.info('usuario con ' + str(registros.count()) + ' registros de login actual')
                for registro in registros:
                    registro.salio = False
                    registro.save()
    except Exception as e:
        logger.exception(e)
    logout(request)
    return redirect(reverse('servicios_app:home'))


@login_required
def perfil_empresa(request):
    template = 'empresa/perfil_empresa.html'
    return render(request, template, {'usuario': request.user,})
