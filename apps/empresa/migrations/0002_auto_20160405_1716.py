# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('empresa', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='empresa',
            name='creador',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='administrativos',
            name='empresa',
            field=models.ForeignKey(to='empresa.Empresa'),
        ),
        migrations.AddField(
            model_name='administrativos',
            name='monitor',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
