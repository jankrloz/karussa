# -*- encoding: utf-8 -*-
from django.shortcuts import redirect

from apps.empresa.estructuras import DatosEmpresaEstrucutra, DatosMonitorTemp
from apps.empresa.models import Empresa
from apps.logger.funciones import logger
from apps.servicios.models import Integrantes


def CargarDatos(models):
    base = DatosEmpresaEstrucutra()
    if type(models) == Empresa:
        base.nombre = models.nombre
        base.correo = models.correo
        base.logo = models.logo
        base.telefono = models.telefono
        base.ciudad = models.ciudad
        base.estado = models.estado
    else:
        base.nombre = models.nombre_empresa
        base.telefono = models.telefono
        base.correo = models.email
    return base


def cargarMonitores(monitores):
    lista_monitores = []
    for monitor in monitores:
        estructura = DatosMonitorTemp()
        estructura.email = monitor.monitor.email
        estructura.username = monitor.monitor.username
        lista_monitores.append(estructura)
    return lista_monitores


# the decorator
def login_required_custom(f):
    def wrap(request, *args, **kwargs):
        if 'userid' not in request.session.keys():
            logger.info(vars(request))
            logger.info(args)
            logger.info(kwargs)
            return redirect('/')
        return f(request, *args, **kwargs)

    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap


def dame_empresa_por_slug(slug):
    try:
        return Empresa.objects.get(slug=slug)
    except:
        return None


def crear_administrativo_empresa(paquete, padre, hijo, tipo):
    if Integrantes.objects.filter(paquete=paquete, nivel=tipo).count() < paquete.monitores:
        if not Integrantes.objects.filter(paquete=paquete, integrante=hijo.email, nivel=tipo).exists():
            Integrantes.objects.create(
                    paquete=paquete,
                    monitor=padre.email,
                    integrante=hijo.email,
                    nivel=tipo
            )
            # mandar Correos


def verificar_existe_administrador(paquete, monitor, creador):
    return Integrantes.objects.filter(
            paquete=paquete,
            integrante=monitor.email,
            monitor=creador.email,
            nivel='monitor',
            estado='Aceptado'
    ).exists()


def buscarEmpresaPorUsuario(usuario):
    slug = ""
    try:
        if usuario.isCreador:
            if Integrantes.objects.filter(monitor=usuario.email, nivel='creador', estado='Aceptado').exists():
                paquete = Integrantes.objects.get(monitor=usuario.email, nivel='creador', estado='Aceptado').paquete
                slug = paquete.empresa.slug
        elif usuario.isMonitor:
            if Integrantes.objects.filter(monitor=usuario.email, nivel='monitor', estado='Aceptado').exists():
                paquete = Integrantes.objects.get(monitor=usuario.email, nivel='monitor', estado='Aceptado').paquete
                slug = paquete.empresa.slug
        elif usuario.isGodinez:
            if Integrantes.objects.filter(monitor=usuario.email, nivel='godinez', estado='Aceptado').exists():
                paquete = Integrantes.objects.get(monitor=usuario.email, nivel='godinez', estado='Aceptado').paquete
                slug = paquete.empresa.slug
    except Exception as e:
        logger.exception(e.args)
    return slug
