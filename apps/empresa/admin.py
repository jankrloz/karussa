# -*- encoding: utf-8 -*-

from django.contrib.admin import register
from nested_inline import admin

from apps.empresa.models import Empresa
from apps.servicios.models import IntegratesAdministrativos, Paquete, IntegrantesGodinez


class AdministrativosInline(admin.NestedInline):
    model = IntegratesAdministrativos
    extra = 0
    # fields = ('monitor', 'estado')
    # readonly_fields = ('monitor', 'estado')


class GodinezInline(admin.NestedInline):
    model = IntegrantesGodinez
    extra = 0

    # fields = ('monitor', 'estado')
    # readonly_fields = ('monitor', 'estado')


class PaqueteInline(admin.NestedStackedInline):
    model = Paquete
    extra = 0
    inlines = [AdministrativosInline, GodinezInline]

    # fields = ('monitor', 'estado')
    # readonly_fields = ('monitor', 'estado')


@register(Empresa)
class AdminEmpresa(admin.NestedModelAdmin):
    list_display = ('nombre', 'creador', 'estado', 'ciudad', 'slug')
    # readonly_fields = ('nombre',)
    fields = ('creador', 'nombre', 'estado', 'ciudad', 'telefono', 'correo', 'logo')
    inlines = [PaqueteInline, ]
