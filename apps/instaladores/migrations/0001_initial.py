# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import apps.instaladores.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Instalador',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('archivo', models.FileField(upload_to=apps.instaladores.models.instaldor_path, max_length=1000)),
                ('tipo', models.CharField(max_length=30, choices=[('linux', 'linux'), ('MAC', 'MAC'), ('windows', 'windows')])),
                ('titulo', models.CharField(default='', max_length=30)),
            ],
        ),
    ]
