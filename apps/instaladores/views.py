from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.http.response import Http404
from django.shortcuts import render

# Create your views here.

#### imports para jugar con archivos y descargarlos

from apps.instaladores.models import Instalador
from apps.logger.funciones import logger


@login_required
def descargar_instalador(request):
    return render(request, 'instaladores/descargar.html')


@login_required
def post_descargar_instalador(request):
    if "SO" in request.POST:
        try:
            archivo = Instalador.objects.get(tipo=request.POST["SO"]).archivo._get_file()
            response = HttpResponse(content_type='application/x-rar-compressed')
            response['Content-Disposition'] = 'attachment; filename="karussapp-{0}"'.format(request.POST['SO'] + '.rar')
            response.write(archivo.read())
            return response
        except Exception as e:
            logger.exception(e)
    raise Http404("no existe ese instalador")
