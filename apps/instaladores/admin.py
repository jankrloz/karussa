from django.contrib import admin

# Register your models here.
from apps.instaladores.models import Instalador


@admin.register(Instalador)
class AdminInstalador(admin.ModelAdmin):
    pass
