# -*- encoding: utf-8 -*-
from django.conf.urls import url

urlpatterns = [
    # url(r'^$', 'apps.instaladores.views.test_karussa', name='test_karussa'),
    url(r'^descargar$', 'apps.instaladores.views.descargar_instalador', name='descargar_instalador'),
    url(r'^descargar_instalador$', 'apps.instaladores.views.post_descargar_instalador',
        name='post_descargar_instalador'),
]
