# -*- encoding: utf-8 -*-

from django.contrib import admin

from apps.estadisticas.models import Historial


@admin.register(Historial)
class AdminHistorial(admin.ModelAdmin):
    pass
    '''
    list_display = ('usuario', 'video', 'fecha')
    fields = ('usuario', 'video', 'fecha', 'postergar')
    readonly_fields = ('usuario', 'video', 'postergar')
    '''
