# -*- encoding: utf-8 -*-

from swampdragon import route_handler
from swampdragon.message_format import format_message
from swampdragon.permissions import LoginRequired
from swampdragon.pubsub_providers.model_channel_builder import make_channels
from swampdragon.route_handler import ModelPubRouter, BaseRouter, CHANNEL_DATA_SUBSCRIBE
from apps.empresa.funciones import dame_empresa_por_slug
from apps.empresa.models import Empresa
from apps.notificaciones.funciones import broadcast_sys_info, crear_notificacion_para_usuario
from apps.servicios.models import Integrantes
from .models import Notification
from .serializers import NotificationSerializer


from swampdragon.route_handler import ModelRouter

#class NotificationRouter(ModelRouter):
class NotificationRouter(ModelPubRouter):
    valid_verbs = ['get_list', 'send_two_messages' ,'get_single', 'create', 'update', 'delete', 'subscribe', 'unsubscribe','molestar_usuario']

    #valid_verbs = ['subscribe','get_single']
    route_name = 'karussa_notificaciones'
    model = Notification
    serializer_class = NotificationSerializer

    def molestar_usuario(self, **kwargs):
        try:
            logger.info("MOLESTAR_USUARIO ------------")
            #channel = self._get_channel_name()
            logger.info(self)
            logger.info(vars(self))
            logger.info("metodos de self")
            metodos = [method for method in dir(self) if callable(getattr(self, method))]
            logger.info(metodos)

            logger.info(vars(self.connection))
            logger.info("metodos de self.conenction")
            metodos = [method for method in dir(self.connection) if callable(getattr(self.connection, method))]
            logger.info(metodos)

            logger.info(vars(self.connection.session_store))
            logger.info("metodos de self.connection.session_store")
            metodos = [method for method in dir(self.connection.session_store) if callable(getattr(self.connection.session_store, method))]
            logger.info(metodos)

            logger.info(vars(self.connection.pub_sub))
            logger.info("metodos de self.connection.pub_sub")
            metodos = [method for method in dir(self.connection.pub_sub) if callable(getattr(self.connection.pub_sub, method))]
            logger.info(metodos)

            #channel = self.connection.pub_sub.get_channel()
            #logger.info("canal : ",str(channel))
            logger.info(kwargs)
            if (('id_usuario' not in kwargs) or ('slug_empresa'not in kwargs)):
                self.send({'status': False})
            else:
                #CREAMOS LA NOTIFICACION
                crear_notificacion_para_usuario(kwargs['id_usuario'], kwargs['slug_empresa'])

                self.send({'message': 'exito al enviar la notificacion'})
            #self.publish([channel], {'some_data': 'another value'})
        except Exception as e:
            logger.info(e)



    def send_two_messages(self, **kwargs):
        try:
            logger.info("SEND_TWO_MESSAGES____")
            #channel = self._get_channel_name()


            logger.info(vars(self.connection))
            logger.info("metodos de self.conenction")
            metodos = [method for method in dir(self.connection) if callable(getattr(self.connection, method))]
            logger.info(metodos)

            logger.info(vars(self.connection.pub_sub))
            logger.info("metodos de self.connection.pub_sub")
            metodos = [method for method in dir(self.connection.pub_sub) if callable(getattr(self.connection.pub_sub, method))]
            logger.info(metodos)

            logger.info(kwargs)
            self.send({'some_data': 'a value'})
            #self.publish([channel], {'some_data': 'another value'})
        except Exception as e:
            logger.info(e)

    def get_query_set(self, **kwargs):
        logger.info("get_query_set_________________-")
        #logger.info(self)
        #logger.info(kwargs)
        return self.model.objects.filter(user=self.connection.user)

    def get_object(self, **kwargs):
        logger.info("get_object____________________")
        #logger.info(self)
        #logger.info(kwargs)
        #return self.model.objects.get(user=self.connection.user , pk=kwargs['pk'])
        return self.model.objects.get(user=self.connection.user , pk=kwargs['pk'])

    def get_subscription_channels(self, **kwargs):
            logger.info("GET_SUBSCRIPTION_CHANNELS_______________")
            #broadcast_sys_info()
            logger.info(self)
            logger.info(vars(self))
            logger.info(kwargs)
            return ['notifications']

    def subscribe(self, **kwargs):
        client_channel = kwargs.pop('channel')
        server_channels = make_channels(self.serializer_class, self.include_related, self.get_subscription_contexts(**kwargs))
        data = self.serializer_class.get_object_map(self.include_related)
        channel_setup = self.make_channel_data(client_channel, server_channels, CHANNEL_DATA_SUBSCRIBE)
        self.send(
            data=data,
            channel_setup=channel_setup,
            **kwargs
        )
        self.connection.pub_sub.subscribe(server_channels, self.connection)
        #broadcast_sys_info()

        logger.info("SUBSCRIBE_____")
        logger.info("KWARGS")
        logger.info(kwargs)

        logger.info("SELF_SERIALIZER_CLASS")
        logger.info(vars(self.serializer_class))
        logger.info("SELF._INCLUDE_RELATED")
        logger.info(self.include_related)

        logger.info("CLIENT_CHANNEL")
        logger.info(client_channel)
        logger.info("SERVER_CHANNELS")
        logger.info(server_channels)
        logger.info("DATA")
        logger.info(data)
        logger.info("CHANNEL_SETUP")
        logger.info(channel_setup)
        logger.info("SELF AL FINAL")
        logger.info(vars(self.connection))


    def get_subscription_contexts(self, **kwargs):
        #broadcast_sys_info()
        logger.info("get_subscription_contexts______________________")
        logger.info(""+str(vars(self)))
        logger.info("kwargs__GSC")
        logger.info(kwargs)
        #logger.info(kwargs[])
        logger.info("vars(self.connection)__")
        logger.info(vars(self.connection))
        contexto = dict()

        if 'slug_empresa' in kwargs:
            slug_empresa = kwargs['slug_empresa']
            logger.info(slug_empresa)
            #obtenemos la empresa
            empresa = dame_empresa_por_slug(slug_empresa)
            if empresa is not None:
                logger.info("SI EXISTIO EMPRESA ")
                '''
                contexto['empresa__id'] = empresa.id
                if empresa.id == 2:
                    contexto['verb'] = 'admin'
                else:
                    contexto['verb'] = 'consola'
                '''
        contexto['user__id'] = self.connection.user.pk
        logger.info("contexto = ",contexto)
        return contexto


    def get_client_context(self, verb, **kwargs):
        logger.info("get_client_context________________________")
        return {'id__': self.connection.user.pk}

    def send(self, data, channel_setup=None, **kwargs):
        logger.info("metodo send______________________-")
        self.context['state'] = 'success'
        if 'verb' in self.context:
            client_context = self.get_client_context(self.context['verb'], **kwargs)
            self._update_client_context(client_context)
            logger.info("Client_context : "+str(client_context))

        message = format_message(data=data, context=self.context, channel_setup=channel_setup)
        #logger.info("se enviara el mensaje "+str(message))
        self.connection.send(message)
        logger.info("se ENVIO mensaje "+str(message))
# ROUTER
route_handler.register(NotificationRouter)



'''

from swampdragon import route_handler
from swampdragon.route_handler import BaseRouter

class FooRouter(BaseRouter):
    valid_verbs = ['do_foo', 'subscribe']
    route_name = 'foo'

    def get_subscription_channels(self, **kwargs):
        return ['foo']

    def do_foo(self, bar):
        self.publish(['foo'], {'hello': 'world'})



class NotificationRouter(ModelPubRouter):
    valid_verbs = ['subscribe']
    #valid_verbs = ['subscribe','get_single']
    #Nombre del router
    route_name = 'notificaciones'
    model = Notificacion
    serializer_class = NotificacionSerializer

    def get_query_set(self, **kwargs):
        logger.info("get_query_set_________________-")
        logger.info(self)
        logger.info(kwargs)
        return self.model.objects.filter(user=self.connection.user)

    def get_object(self, **kwargs):
        logger.info("get_object____________________")
        logger.info(self)
        logger.info(kwargs)
        return self.model.objects.get(user=self.connection.user, pk=kwargs['pk'])

    def get_subscription_contexts(self, **kwargs):
        try:
            logger.info("get_subscription_contexts______________________")
            logger.info(self)
            logger.info(""+str(vars(self)))
            logger.info("conection.sesion "+str(vars(self.connection.session)))
            logger.info("conection.session_store,conection.pub_sub._subscriber "+str(vars(self.connection.session_store.connection.pub_sub._subscriber )))
            logger.info("self.conection.user "+ str(self.connection.user))
        except Exception as e:
            logger.info(e)
        return {'user__id': self.connection.user.pk}

    def get_client_context(self, verb, **kwargs):
        logger.info("get_client_context________________________")
        logger.info(self)
        logger.info(verb)
        logger.info(kwargs)
        logger.info("self "+str(vars(self)))
        logger.info("conection "+str(vars(self.connection)))
        logger.info("conection.sesion "+str(vars(self.connection.session)))
        logger.info("conection.user "+str(self.connection.user))
        logger.info("verb "+verb)
        return {'user__id': self.connection.user.pk}

    def send(self, data, channel_setup=None, **kwargs):
        logger.info("metodo send______________________-")
        self.context['state'] = 'success'
        if 'verb' in self.context:
            client_context = self.get_client_context(self.context['verb'], **kwargs)
            self._update_client_context(client_context)
            logger.info("Client_context : "+str(client_context))

        message = format_message(data=data, context=self.context, channel_setup=channel_setup)
        logger.info("se enviara el mensaje "+str(message))
        self.connection.send(message)

# 'registramos' el router, para que redis pueda enviar las notificaciones mediante este modelo
route_handler.register(NotificationRouter)

'''
