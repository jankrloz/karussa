from django.conf import settings
from django.db import models
from django.templatetags.static import static
from swampdragon.models import SelfPublishModel
from apps.empresa.models import Empresa
from .serializers import NotificationSerializer

'''
class Notification(SelfPublishModel, models.Model):
    serializer_class = NotificationSerializer
    message = models.TextField()
'''


class Notification(SelfPublishModel, models.Model):
    serializer_class = NotificationSerializer
    message = models.TextField()
    verb = models.CharField(null=True, default="achieved", max_length=20)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    empresa = models.ForeignKey(Empresa, null=True)
