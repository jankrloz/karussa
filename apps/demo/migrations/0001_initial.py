# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import swampdragon.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('message', models.TextField()),
                ('verb', models.CharField(null=True, default='achieved', max_length=20)),
            ],
            bases=(swampdragon.models.SelfPublishModel, models.Model),
        ),
    ]
