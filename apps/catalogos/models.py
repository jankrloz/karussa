# -*- encoding: utf-8 -*-
from datetime import datetime

from django.db import models


# Create your models here.
class CatalogoDia(models.Model):
    nombre = models.CharField(max_length=30)

    def __unicode__(self):
        return self.nombre

    def __str__(self):
        return self.nombre


class DiasFestivos(models.Model):
    nombre = models.CharField(max_length=200)
    fecha = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.nombre + " - " + str(self.fecha)

    def __unicode__(self):
        return self.nombre + " - " + str(self.fecha)
