# -*- encoding: utf-8 -*-
from django.contrib import admin

from apps.catalogos.models import CatalogoDia, DiasFestivos


# Register your models here.
@admin.register(CatalogoDia)
class adminCatalogoDia(admin.ModelAdmin):
    pass


@admin.register(DiasFestivos)
class adminDiasFestivos(admin.ModelAdmin):
    pass
