# -*- encoding: utf-8 -*-
from datetime import date, datetime

from django.conf import settings
from django.contrib.auth import login
from django.contrib.auth import views
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http.response import Http404, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.views.decorators.csrf import csrf_exempt

from apps.empresa.funciones import buscarEmpresaPorUsuario
from apps.empresa.models import Administrativos, Empresa
from apps.empresa.views import logOut
from apps.logger.funciones import logger
from apps.servicios.models import Paquete, Integrantes, VideosPaquete
from apps.usuarios.forms import LoginForm
from apps.usuarios.funciones import cargar_integrante, agregar_godinez, registrar_entrada, agregar_monitores
from apps.usuarios.models import Usuario, EmailOrUsernameModelBackend


@login_required
def vista_monitor(request):
    logger.info('Vista monitor')
    monitor = Integrantes.objects.filter(integrante=request.user.email, estado='Aceptado', nivel='monitor')
    error = ''
    if monitor.exists():
        debug = settings.DEBUG
        paquete = monitor[0].paquete
        usuario = Usuario.objects.get(id=request.user.id)
        if usuario.isMonitor:
            if request.method == 'POST':
                if "lista_correos_nuevos" in request.POST:
                    nuevos_godinez = []
                    lista_correos = request.POST.getlist('lista_correos_nuevos')
                    for correos in lista_correos:
                        for correo in correos.split(','):
                            nuevos_godinez.append(correo)
                    agregar_godinez(nuevos_godinez, paquete.empresa, usuario, paquete)
            try:
                integrantes = Integrantes.objects.filter(paquete=paquete, nivel="godinez")
                cantidad_godinez_libre = paquete.godinez - integrantes.count()
                # cantidad_godinez_libre = paquete.plan.cantidad - integrantes.count()
                lista_integrantes = []
                if integrantes.exists():
                    lista_integrantes = cargar_integrante(integrantes, lista_integrantes)
                return render(request, "usuarios/vista_monitor.html",
                              {'integrantes': lista_integrantes,
                               'nombre_empresa': paquete.empresa.nombre,
                               'usuarios_disponibles': cantidad_godinez_libre,
                               'DEBUG': debug})
            except Exception as e:
                error = e
                logger.exception(e)
    raise Http404(error)


@login_required
def vista_godinez(request):
    try:
        user = request.user
        integrantes = Integrantes.objects.filter(integrante=user.email, paquete__activo=True)
        if integrantes.exists():
            paquete = integrantes[0].paquete
            videos = VideosPaquete.objects.filter(paquete=paquete, fecha=date.today()).order_by("inicia")
            hora = datetime.today().time()
            return render(request, 'usuarios/vista_godinez.html', {
                'videos': videos,
                "hora": hora})
        return render(request, 'usuarios/fuera_de_servicio.html')
    except Exception as e:
        logger.exception(e)
        raise Http404(e.args)


def login_normal(request):
    lista_errores = []
    if request.user.is_authenticated():
        usuario = Usuario.objects.get(id=request.user.id)
        if usuario.is_superuser:
            return redirect(reverse('usuarios_app:superuser'))
        if usuario.isCreador:
            return redirect(reverse("usuarios_app:creador"))
        elif usuario.isMonitor:
            return redirect(reverse("usuarios_app:monitor"))
        elif usuario.isGodinez:
            return redirect(reverse("usuarios_app:godinez"))
        else:
            return render(request, 'usuarios/fuera_de_servicio.html')

    login_form = LoginForm()
    if request.method == 'POST':
        login_form = LoginForm(request.POST)
        try:
            if login_form.is_valid():
                logger.info('Form válido')
                autentificar = EmailOrUsernameModelBackend()
                user, error = autentificar.authenticate(
                    username=login_form.cleaned_data['username'].lower().strip().replace(' ', '_'),
                    password=login_form.cleaned_data['password'])
                logger.info("user {0} error : {1}".format(user, error))
                if user is not None and error == 'ok':
                    user.backend = 'django.contrib.auth.backends.ModelBackend'
                    login(request, user)
                    if user.is_superuser:
                        return redirect(reverse('superuser_app:superuser'))
                    registrar_entrada(user)
                    logger.info('usuario existente', user)
                    slug = buscarEmpresaPorUsuario(user)
                    if slug == "":
                        logOut(request)
                    if user.isCreador:
                        return redirect(reverse("usuarios_app:creador"))
                    elif user.isMonitor:
                        return redirect(reverse("usuarios_app:godinez"))
                    elif user.isGodinez:
                        return redirect(reverse("usuarios_app:monitor"))
                    else:
                        return render(request, 'usuarios/fuera_de_servicio.html')
                else:
                    lista_errores.append(error)
                    logger.info('Usuario no existente', user)
                    logger.info('error : ', error)
        except Exception as e:
            lista_errores.append(e)
            logger.exception(e)
    return render(request, 'empresa/login.html',
                  {'login_form': login_form,
                   'errores': lista_errores})


def login_globalv2(request):
    url_redireccion = request.GET['next'] if 'next' in request.GET else '/'
    if request.user.is_authenticated():
        return redirect(url_redireccion)
    else:
        login_form = LoginForm()
        return render(request, 'empresa/login.html',
                      {'login_form': login_form})


def login_global(request):
    lista_errores = []
    url_redireccion = request.GET['next'] if 'next' in request.GET else "/"
    if request.user.is_authenticated():
        usuario = Usuario.objects.get(id=request.user.id)
        logger.info('usuario loggeado')
        logger.info(usuario.is_active)
        logger.info(usuario.id)
        if usuario.is_active:
            if usuario.is_superuser:
                return redirect(reverse('usuarios_app:superuser'))
            if usuario.isCreador:
                return redirect(reverse("usuarios_app:creador"))
            elif usuario.isMonitor:
                return redirect(reverse("usuarios_app:monitor"))
            elif usuario.isGodinez:
                if url_redireccion != "/":
                    return redirect(url_redireccion)
                return redirect(reverse("usuarios_app:godinez"))
            else:
                return render(request, 'usuarios/fuera_de_servicio.html')
        logger.info('se redireccionara al usuario para que cambie su password')
        return redirect(reverse("usuarios_app:change_password"))

    login_form = LoginForm()
    if request.method == 'POST':
        login_form = LoginForm(request.POST)
        try:
            if login_form.is_valid():
                autentificar = EmailOrUsernameModelBackend()
                user, error = autentificar.authenticate(
                    username=login_form.cleaned_data['username'].lower().strip().replace(' ', '_'),
                    password=login_form.cleaned_data['password']
                )
                if user is not None and error == 'ok':
                    user.backend = 'django.contrib.auth.backends.ModelBackend'
                    login(request, user)
                    if user.is_active:
                        if user.is_superuser:
                            return redirect(reverse('superuser_app:superuser'))
                        registrar_entrada(user)
                        if user.isCreador:
                            return redirect(reverse("usuarios_app:creador"))
                        elif user.isMonitor:
                            return redirect(reverse("usuarios_app:monitor"))
                        elif user.isGodinez:
                            if url_redireccion != "/":
                                return redirect(url_redireccion)
                            return redirect(reverse("usuarios_app:godinez"))
                        else:
                            return render(request, 'usuarios/fuera_de_servicio.html')
                    else:
                        return redirect(reverse("usuarios_app:change_password"))
                else:
                    logger.info("error de logeo")
                    lista_errores.append(error)
                    logger.info(error)
        except Exception as e:
            lista_errores.append(e)
            logger.info("error del try")
            logger.info(e)
    return render(request, 'empresa/login.html',
                  {'login_form': login_form,
                   'errores': lista_errores})


# LOGIN CON JAVA
#   Solo se puede loguear por metodo POST
#   debe recibir un email y un password
#   si esta correcto, loggea al usuario
#   y retorna una notificacion al usuario con el nombre del canal, y demas informacion para configuracion
@csrf_exempt
def login_java(request):
    logger.info(request)
    lista_errores = []
    login_form = LoginForm()
    if request.method == 'GET':
        logger.info("Metodo GET en login_java ")
        return HttpResponse("ok")

    if request.method == "POST":
        logger.info("request.POST en login_java")
        logger.info(request.POST)

        login_form = LoginForm(request.POST)
        try:
            if login_form.is_valid():
                logger.info('Form válido')
                autentificar = EmailOrUsernameModelBackend()
                user, error = autentificar.authenticate(
                    username=login_form.cleaned_data['username'].lower().strip().replace(' ', '_'),
                    password=login_form.cleaned_data['password'])
                logger.info("user {0} error : {1}".format(user, error))
                logger.info(user.id)
                return HttpResponse(user.id)

        except Exception as e:
            lista_errores.append(e)
            logger.exception(e)
        return HttpResponse("-1")


'''
def login_personalizado(request, slug):
    empresa = get_object_or_404(Empresa, slug=slug)

    lista_errores = []
    if request.user.is_authenticated():
        usuario = Usuario.objects.get(id=request.user.id)
        if usuario.isCreador:
            return redirect(reverse("usuarios_app:creador", kwargs={'slug': slug}))
        elif usuario.isMonitor:
            return redirect(reverse("usuarios_app:monitor", kwargs={'slug': slug}))
        elif usuario.isGodinez:
            return redirect(reverse("usuarios_app:godinez", kwargs={'slug': slug}))
        else:
            return render(request, 'usuarios/fuera_de_servicio.html')

    login_form = LoginForm()

    if request.method == 'POST':
        if not Empresa.objects.filter(slug=slug).exists():
            logger.exception('el usuario %s intento entrar con el slug %s', (request.user, slug))
            raise Http404
        login_form = LoginForm(request.POST)
        try:
            if login_form.is_valid():
                logger.info('Form válido')
                autentificar = EmailOrUsernameModelBackend()
                user, error = autentificar.authenticate(
                        username=login_form.cleaned_data['username'].lower().strip().replace(' ', '_'),
                        password=login_form.cleaned_data['password'])
                logger.info("user {0} error : {1}".format(user, error))
                if user is not None and error == 'ok':
                    registrar_entrada(user)
                    logger.info('usuario existente', user)
                    user.backend = 'django.contrib.auth.backends.ModelBackend'
                    login(request, user)
                    if user.isCreador:
                        return redirect(reverse("usuarios_app:creador", kwargs={'slug': slug}))
                    elif user.isMonitor:
                        return redirect(reverse("usuarios_app:monitor", kwargs={'slug': slug}))
                    elif user.isGodinez:
                        return redirect(reverse("usuarios_app:godinez", kwargs={'slug': slug}))
                    else:
                        return render(request, 'usuarios/fuera_de_servicio.html')
                else:
                    lista_errores.append(error)
                    logger.info('Usuario no existente', user)
                    logger.info('error : ', error)
        except Exception as e:
            lista_errores.append(e)
            logger.exception(e)
    return render(request, 'empresa/login.html',
                  {'login_form': login_form,
                   'empresa': empresa,
                   'errores': lista_errores,
                   })
'''


@login_required
def vista_creador(request):
    usuario = Usuario.objects.get(id=request.user.id)
    if usuario.isCreador:
        emp = Administrativos.objects.filter(monitor=usuario, estado="creador")
        if emp.exists():
            empresa = emp[0].empresa
            paquetes = Paquete.objects.filter(empresa=empresa)
            for paquete in paquetes:
                paquete.cantidad_usuarios = Integrantes.objects.filter(paquete=paquete, nivel='godinez').count()
                paquete.cantidad_monitores = Integrantes.objects.filter(paquete=paquete, nivel='monitor').count()
            return render(request, "usuarios/vista_creador.html",
                          {"paquetes": paquetes, 'empresa': empresa})
    raise Http404('algo extraño sucedio lo sentimos intente mas tarde')


'''
@csrf_exempt
def ajax_agregar_monitor(request):
    logger.info(request.method)
    logger.info(request.is_ajax())
    return HttpResponse('ok')
'''


@login_required
def configurar_usuarios(request):
    logger.info('agregar: ', request.POST)
    if request.method == "POST":
        usuario = get_object_or_404(Usuario, id=request.user.id)
        empresa = get_object_or_404(Empresa, creador=usuario)
        paquete = get_object_or_404(Paquete, id=int(request.POST['id']))
        if "lista_correos_usuarios" in request.POST:
            nuevos_godinez = []
            lista_correos = request.POST.getlist('lista_correos_usuarios')
            for correos in lista_correos:
                for correo in correos.split(','):
                    nuevos_godinez.append(correo)
            agregar_godinez(nuevos_godinez, empresa, usuario, paquete)
            # CORREO agregar godinez

        if 'lista_correos_monitores' in request.POST:
            nuevos_monitores = []
            lista_correos = request.POST.getlist('lista_correos_monitores')
            for correos in lista_correos:
                for correo in correos.split(','):
                    nuevos_monitores.append(correo)
            agregar_monitores(request.user, nuevos_monitores, paquete)
        integrantes = Integrantes.objects.filter(paquete=paquete, nivel="godinez")
        monitores = Integrantes.objects.filter(paquete=paquete, nivel="monitor")
        cantidad_godinez_libre = paquete.godinez - integrantes.count()
        cantidad_monitores_libre = paquete.monitores - monitores.count()
        lista_integrantes = []
        # if integrantes.exists():
        lista_integrantes = cargar_integrante(integrantes, lista_integrantes)
        return render(request, "usuarios/agregar_equipo.html",
                      {'integrantes': lista_integrantes,
                       'monitores': monitores,
                       'usuarios_disponibles': cantidad_godinez_libre,
                       'monitores_disponibles': cantidad_monitores_libre,
                       'id': paquete.id})
        # return render(request, "usuarios/configurar_usuarios.html")
    return redirect(reverse("usuarios_app:creador"))


@login_required
@csrf_exempt
def eliminar_usuario(request):
    if request.is_ajax():
        logger.info(request.POST)
        if 'paquete' in request.POST and 'usuario_muerto' in request.POST:
            try:
                godinez = Usuario.objects.get(id=int(request.POST['usuario_muerto']))
                integrante = Integrantes.objects.get(paquete__id=request.POST['paquete'], integrante=godinez.email,
                                                     nivel='godinez')
                if integrante:
                    integrante.estado = 'Eliminado'
                    integrante.save()
                    godinez.isGodinez = False
                    godinez.save()

                    return HttpResponse('ok')

            except Exception as e:
                logger.exception(e)

        if 'paquete' in request.POST and 'monitor_muerto' in request.POST:
            try:
                monitor = Usuario.objects.get(email=request.POST['monitor_muerto'])
                logger.info('paquete: ', int(request.POST['paquete']))
                logger.info('integrante: ', monitor.email)
                integrante = Integrantes.objects.get(paquete__id=int(request.POST['paquete']), integrante=monitor.email,
                                                     nivel='monitor')
                if integrante:
                    integrante.estado = 'Eliminado'
                    integrante.save()
                    monitor.isMonitor = False
                    monitor.save()

                return HttpResponse('ok')

            except Exception as e:
                logger.exception(e)


# Cambio de password,
def change_password(request):
    if not request.user.is_active:
        return views.password_change(request, template_name='usuarios/cambiar_password.html',
                                     post_change_redirect=reverse(
                                         'usuarios_app:password_change_done'))  # post_change_redirect=
    else:
        return redirect(reverse('usuarios_app:login'))


# Post-cambio de password, se hara activo al usuario
def password_change_done(request):
    # logger.info('se logro cambiar correctamente el Password, activaremos al usuario')
    # logger.info(request.user.username)
    # logger.info(request.user.is_active)
    request.user.is_active = True
    request.user.save()
    return redirect(reverse('usuarios_app:login'))
