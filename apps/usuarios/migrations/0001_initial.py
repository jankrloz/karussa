# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('password', models.CharField(verbose_name='password', max_length=128)),
                ('last_login', models.DateTimeField(verbose_name='last login', blank=True, null=True)),
                ('is_superuser', models.BooleanField(help_text='Designates that this user has all permissions without explicitly assigning them.', default=False, verbose_name='superuser status')),
                ('username', models.CharField(unique=True, max_length=40)),
                ('nombre', models.CharField(max_length=40, blank=True)),
                ('apellidos', models.CharField(max_length=40, blank=True)),
                ('fecha_registro', models.DateTimeField(auto_now_add=True)),
                ('email', models.EmailField(unique=True, max_length=254)),
                ('status', models.BooleanField(default=False)),
                ('isCreador', models.BooleanField(default=False)),
                ('isMonitor', models.BooleanField(default=False)),
                ('isGodinez', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=True)),
                ('is_staff', models.BooleanField(default=False)),
                ('groups', models.ManyToManyField(verbose_name='groups', to='auth.Group', related_name='user_set', help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_query_name='user', blank=True)),
                ('user_permissions', models.ManyToManyField(verbose_name='user permissions', to='auth.Permission', related_name='user_set', help_text='Specific permissions for this user.', related_query_name='user', blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RegistroEntrada',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('fecha_login', models.DateTimeField(auto_now_add=True)),
                ('salio', models.BooleanField(default=False)),
                ('navegador', models.CharField(max_length=20)),
                ('ip', models.CharField(max_length=20)),
                ('so', models.CharField(max_length=20)),
                ('dispositivo', models.CharField(max_length=20)),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
