from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from apps.logger.funciones import logger
from django.conf import settings


def correo_admin_empresa(admin, empresa, password):
    subject = "Has sido asignado como administrador en Karussa"
    from_email = "KARUSSA <somosrenka@gmail.com>"
    to = [admin.email, ]
    url_accion = "http://104.131.146.146" + reverse('usuarios_app:login')

    ctx = {'nombre_empresa': empresa.nombre,
           'asunto': subject,
           'correo_admin': admin.email,
           'password_admin': password,
           'url_accion': url_accion}

    html_content = render_to_string("correos/correo-admin-empresa.html", context=ctx)
    text_content = strip_tags(html_content)

    msg = EmailMultiAlternatives(subject, text_content, from_email, to, cc=['jankrloz.navarrete@gmail.com'])
    msg.attach_alternative(html_content, "text/html")
    try:
        msg.send()
    except Exception as e:
        logger.error("error por " + str(e))

        # return render(request, 'correos/correo-admin-empresa.html', ctx)


### correo de que ha sido creado el paquete
def correo_paquete_creado(paquete, asunto='Paquete creado | KARUSSA YOGA', destino=["", ]):
    try:
        subject = asunto
        from_email = "KARUSSA <somosrenka@gmail.com>"
        to = destino
        host = settings.HOST
        #   url_accion llevara al usuario a subir un proyecto
        url_accion = host + reverse('usuarios_app:login')

        ctx = {'url_accion': url_accion}

        html_content = render_to_string("correos/correo_paquete_creado.html", context=ctx)
        text_content = strip_tags(html_content)

        msg = EmailMultiAlternatives(subject, text_content, from_email, to)
        msg.attach_alternative(html_content, "text/html")

        msg.send()

    except Exception as e:
        logger.exception(e)


### correo maanana inicia tu paquete
def correo_manana_inicia_paquete(paquete, asunto='Mañana inicia tu paquete | KARUSSA YOGA',
                                 destino=['karussa.yoga@gmail.com', ]):
    try:
        subject = asunto
        from_email = "KARUSSA YOGA <somosrenka@gmail.com>"
        to = destino
        host = settings.HOST
        url_accion = host + reverse('usuarios_app:login')

        ctx = {'url_accion': url_accion}

        html_content = render_to_string("correos/correo_manana_inicia_tu_paquete.html", context=ctx)
        text_content = strip_tags(html_content)

        msg = EmailMultiAlternatives(subject, text_content, from_email, to)
        msg.attach_alternative(html_content, "text/html")

        msg.send()
    except Exception as e:
        logger.info(e)
