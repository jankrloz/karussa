# -*- encoding: utf-8 -*-

from django.contrib import admin
from apps.videos.models import Video


@admin.register(Video)
class AdminVideos(admin.ModelAdmin):
    pass
