# -*- encoding: utf-8 -*-

from django.db import models

# Create your models here.
from apps.logger.funciones import logger


def videos_path(instance, filename):
    return "videos/{id}/{file}".format(id=instance.id, file=filename)


class Video(models.Model):
    titulo = models.CharField(max_length=50)
    descripcion = models.TextField(blank=True, null=True)
    archivo = models.FileField()  # upload_to=videos_path)

    def dame_url_video(self):
        try:
            return self.archivo.url
        except Exception as e:
            logger.exception(e)
            return False

    def __str__(self):
        return self.titulo

    def __unicode__(self):
        return self.titulo
