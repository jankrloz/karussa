from apps.empresa.models import Empresa
from apps.logger.funciones import logger
from apps.servicios.models import Paquete
from apps.usuarios.models import Usuario


def validarGodinez(id, llave, slug):
    if Usuario.objects.filter(id=id).exists():
        usuario = Usuario.objects.get(id=id)
        if usuario.isGodinez:
            if Empresa.objects.filter(slug=slug).exists():
                try:
                    empresa = Empresa.objects.get(slug=slug)
                    paquete = Paquete.objects.get(empresa=empresa)
                    plan = paquete.plan
                    '''
                    if VideosDia.objects.filter(plan=plan, llave=llave).exists():
                        Integrantes.objects.get(paquete=paquete, integrante=usuario.email)
                        return True
                    '''
                except Exception as e:
                    logger.exception(e)
    return False
