# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('titulo', models.CharField(max_length=50)),
                ('descripcion', models.TextField(blank=True, null=True)),
                ('archivo', models.FileField(upload_to='')),
            ],
        ),
    ]
