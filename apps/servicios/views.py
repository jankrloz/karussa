# -*- encoding: utf-8 -*-
from datetime import date

from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect

from apps.empresa.funciones import CargarDatos, crear_administrativo_empresa, verificar_existe_administrador
from apps.empresa.models import Empresa
from apps.logger.funciones import logger
from apps.servicios.correos import email_nuevo_contacto
from apps.servicios.forms import CodigoForm
from apps.servicios.forms import ContactoForm
from apps.servicios.funciones import funcion_cargar_plan_estructura, genera_cadena_aleatorio, crear_seleccion_paquete
from apps.servicios.models import Plan, Contacto
from apps.usuarios.funciones import guardar_usuario
from apps.usuarios.models import Usuario


def home(request):
    return render(request, 'servicios/lista_planes.html')
    # return redirect(reverse('servicios_app:planes'))


def planes(request):
    planes_objetos = Plan.objects.filter(visible=True)
    planes = []
    for plan in planes_objetos:
        planes.append(funcion_cargar_plan_estructura(plan))

    if request.method == 'POST':
        if request.POST['plan_seleccionado']:
            plan_seleccionado = request.POST['plan_seleccionado']
            request.session['plan_seleccionado'] = plan_seleccionado
            logger.info('PLAN ENVIADO A SESION')
            return redirect(reverse('servicios_app:form_contacto'))

    return render(request, 'servicios/lista_planes.html', {'usuario': request.user,
                                                           'planes': planes})


def form_contacto(request):
    logger.info("FORMULARIO DE CONTACTO")
    template = 'servicios/form_contacto.html'

    contacto_form = ContactoForm()

    return render(request, template, {'contacto_form': contacto_form})


def finalizar_contacto(request):
    if request.method == 'POST':
        template = 'servicios/finalizar_contacto.html'
        if 'contacto_form' in request.POST:
            post = request.POST.copy()
            # nuevoPlan = funcionRemplazarPlan(request.POST['plan'], post['nombre_contacto'])
            # post.__setitem__('plan', nuevoPlan)
            logger.info(post)
            contacto_form = ContactoForm(post)
            if contacto_form.is_valid():
                logger.info("formulario valido")
                contacto_form.save()
                return render(request, template, {'usuario': request.user,
                                                  'nombre_contacto': request.POST['nombre_contacto'],
                                                  'nombre_empresa': request.POST['nombre_empresa'],
                                                  'email': request.POST['email'],
                                                  'telefono': request.POST['telefono']
                                                  })

    return redirect(reverse('servicios_app:contacto'))


def configurar(request):
    codigo_form = CodigoForm()
    if request.method == "POST":
        if 'codigo' in request.POST:
            codigo_form = CodigoForm(request.POST)
            if codigo_form.is_valid():
                try:
                    codigo = codigo_form.cleaned_data['codigo']
                    plan = Plan.objects.get(id=Contacto.objects.get(random=codigo).plan.id)
                    try:
                        empresa = Empresa.objects.get(codigo=codigo)
                        return redirect(reverse('usuarios_app:login'))
                    except Exception as e:
                        # logger.exception(e)
                        datos = CargarDatos(Contacto.objects.get(random=codigo))
                        monitores = []
                        for x in range(plan.monitores):
                            monitores.append(None)
                        return render(request, 'servicios/configurar.html',
                                      {'valido': True,
                                       'plan': plan,
                                       'codigo_form': codigo_form,
                                       'codigo': codigo,
                                       'monitores': monitores,
                                       'datos': datos})
                except Exception as e:
                    logger.exception(e)
                    return render(request, 'servicios/configurar.html',
                                  {'codigo_form': codigo_form,
                                   'error': "Verifica el código de acceso"})
            else:
                return render(request, 'servicios/configurar.html',
                              {'codigo_form': codigo_form,
                               'error': "Por favor, revisa que los datos sean correctos."})
        elif 'codigo2' in request.POST:
            codigo = request.POST['codigo2']
            empresa = Empresa.objects.filter(codigo=codigo)
            if empresa.exists():
                return redirect(reverse('usuarios_app:login'))
            elif 'pass_creador' in request.POST and 'email_creador' in request.POST and request.POST[
                'pass_creador'] != '' and request.POST['email_creador'] != '':
                if not Usuario.objects.filter(email=request.POST['email_creador']).exists():
                    try:
                        creador = Usuario.objects.create_user(
                            username=request.POST['email_creador'],
                            email=request.POST['email_creador'],
                            password=request.POST['pass_creador'],
                        )
                        creador.isCreador = True
                        creador.save()

                        plan = Plan.objects.get(id=Contacto.objects.get(random=codigo).plan.id)
                        empresa = Empresa()
                        if 'nombre_empresa' in request.POST:
                            empresa.nombre = request.POST['nombre_empresa']
                        # if 'ubicacion_empresa' in request.POST:
                        #    empresa.ubicacion = request.POST['ubicacion_empresa']
                        if 'telefono_empresa' in request.POST:
                            empresa.telefono = request.POST['telefono_empresa']
                        if 'correo_empresa' in request.POST:
                            empresa.correo = request.POST['correo_empresa']
                        if 'logo_empresa' in request.POST:
                            empresa.logo = request.POST['logo_empresa']
                        empresa.codigo = codigo
                        empresa.save()
                        hoy = date.today()
                        paquete = crear_seleccion_paquete(empresa, plan, creador, hoy)
                        crear_administrativo_empresa(paquete, creador, creador, "creador")
                        if 'email_monitores' in request.POST:
                            for monitor in request.POST.getlist('email_monitores'):
                                if monitor != "":
                                    username = str(monitor).strip()
                                    email = username
                                    monitorExiste = False
                                    if Usuario.objects.filter(email=email).exists():
                                        nuevo_monitor = Usuario.objects.get(email=email)
                                        monitorExiste = verificar_existe_administrador(paquete, nuevo_monitor, creador)
                                    else:
                                        password = genera_cadena_aleatorio(6)
                                        nuevo_monitor = Usuario.objects.create_user(
                                            username=username,
                                            email=email,
                                            password=password
                                        )
                                        guardar_usuario('monitor', username, password)
                                    if not monitorExiste:
                                        nuevo_monitor.isMonitor = True
                                        nuevo_monitor.save()
                                        crear_administrativo_empresa(empresa, creador, nuevo_monitor, "monitor")

                            # aqui mandamos el correo al nuevo usuario
                            '''
                            Integrantes.objects.create(
                                paquete=paquete,
                                integranteEstructura=nuevo_monitor.email,
                                monitor=creador.email
                            )
                            '''
                        return redirect(reverse('usuarios_app:login'))

                    except Exception as e:
                        logger.exception(e)
                        return render(request, 'servicios/configurar.html', {'codigo_form': codigo_form,
                                                                             'error2': e.args})
    else:
        return render(request, 'servicios/configurar.html', {'codigo_form': codigo_form})


def configurar_paquete(request):
    codigo_form = CodigoForm()
    if request.method == "POST":
        if 'codigo2' in request.POST:
            codigo = request.POST['codigo2']
            empresa = Empresa.objects.filter(codigo=codigo)
            if empresa.exists():
                return redirect(reverse('usuarios_app:login'))
            elif 'pass_creador' in request.POST and 'email_creador' in request.POST and request.POST[
                'pass_creador'] != '' and request.POST['email_creador'] != '':
                if not Usuario.objects.filter(email=request.POST['email_creador']).exists():
                    try:
                        creador = Usuario.objects.create_user(
                            username=request.POST['email_creador'],
                            email=request.POST['email_creador'],
                            password=request.POST['pass_creador'],
                        )
                        creador.isCreador = True
                        creador.save()

                        plan = Plan.objects.get(id=Contacto.objects.get(random=codigo).plan.id)
                        empresa = Empresa()
                        if 'nombre_empresa' in request.POST:
                            empresa.nombre = request.POST['nombre_empresa']
                        # if 'ubicacion_empresa' in request.POST:
                        #    empresa.ubicacion = request.POST['ubicacion_empresa']
                        if 'telefono_empresa' in request.POST:
                            empresa.telefono = request.POST['telefono_empresa']
                        if 'correo_empresa' in request.POST:
                            empresa.correo = request.POST['correo_empresa']
                        if 'logo_empresa' in request.POST:
                            empresa.logo = request.POST['logo_empresa']
                        empresa.codigo = codigo
                        empresa.save()
                        hoy = date.today()
                        paquete = crear_seleccion_paquete(empresa, plan, creador, hoy)
                        crear_administrativo_empresa(paquete, creador, creador, "creador")
                        if 'email_monitores' in request.POST:
                            for monitor in request.POST.getlist('email_monitores'):
                                if monitor != "":
                                    username = str(monitor).strip()
                                    email = username
                                    monitorExiste = False
                                    if Usuario.objects.filter(email=email).exists():
                                        nuevo_monitor = Usuario.objects.get(email=email)
                                        monitorExiste = verificar_existe_administrador(paquete, nuevo_monitor, creador)
                                    else:
                                        password = genera_cadena_aleatorio(6)
                                        nuevo_monitor = Usuario.objects.create_user(
                                            username=username,
                                            email=email,
                                            password=password
                                        )
                                        guardar_usuario('monitor', username, password)
                                    if not monitorExiste:
                                        nuevo_monitor.isMonitor = True
                                        nuevo_monitor.save()
                                        crear_administrativo_empresa(empresa, creador, nuevo_monitor, "monitor")

                            # aqui mandamos el correo al nuevo usuario
                            '''
                            Integrantes.objects.create(
                                paquete=paquete,
                                integranteEstructura=nuevo_monitor.email,
                                monitor=creador.email
                            )
                            '''
                        return redirect(reverse('usuarios_app:login'))

                    except Exception as e:
                        logger.exception(e)
                        return render(request, 'servicios/configurar-paquete.html', {'codigo_form': codigo_form,
                                                                                     'error2': e.args})
    else:
        return render(request, 'servicios/configurar-paquete.html', {'codigo_form': codigo_form})


'''
@login_required
def iniciar_paquete(request):
    try:
        if request.method == 'POST':
            logger.info(request.POST)
            if 'activar' in request.POST and 'slug' in request.POST:
                slug = request.POST['slug']
                empresa = Empresa.objects.get(slug=slug)
                usuario = request.user
                if Integrantes.objects.filter(monitor=usuario.email, nivel='creador', estado='Aceptado').exists():
                    # if Administrativos.objects.filter(empresa=empresa, monitor=request.user).exists():
                    paquete = Integrantes.objects.get(monitor=usuario.email, nivel='creador', estado='Aceptado').paquete
                    paquete.fecha_inicio = date.today()
                    paquete.fecha_fin = date.today() + relativedelta(month=paquete.plan.duracion)
                    paquete.save()
                    #activarFechas(paquete, date.today())
                    return redirect(reverse("usuarios_app:creador", kwargs={'slug': slug}))
    except Exception as e:
        logger.exception(e)
        logger.info(e)
    raise Http404('algo')

@login_required
def actualizar_hora(request):
    logger.info(request.POST)
    if 'slug' in request.POST and 'horarioInicia' in request.POST and 'horarioFin' in request.POST:
        try:
            slug = request.POST['slug']
            empresa = Empresa.objects.get(slug=slug)
            horaI = int(request.POST['horarioInicia'])
            horaF = int(request.POST['horarioFin'])
            usuario = request.user
            if Integrantes.objects.filter(monitor=usuario.email, nivel='creador', estado='Aceptado').exists():
                paquete = Integrantes.objects.get(monitor=usuario.email, nivel='creador', estado='Aceptado').paquete
                #asignarHorario(paquete, horaI, horaF)
                return redirect(reverse("usuarios_app:creador", kwargs={'slug': slug}))
        except Exception as e:
            logger.exception(e.args)
    raise Http404('se muere D:')
    '''
