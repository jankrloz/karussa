# -*- encoding: utf-8 -*-
from django import forms
from apps.logger.funciones import logger

from apps.servicios.models import Contacto
from apps.usuarios.models import Usuario


class ContactoForm(forms.ModelForm):
    class Meta:
        model = Contacto
        fields = [
            'nombre_contacto',
            'nombre_empresa',
            'email',
            'telefono',
            'extension',
            'telefono_adicional',
            'cantidad_empleados',
        ]

    email = forms.CharField(
        max_length=50,
        required=True,
        label='Email de contacto (Requerido)',
        widget=forms.TextInput(attrs={
            'type': 'email',
            'class': 'form-control',
            'required': 'true',
        }))

    nombre_contacto = forms.CharField(
        max_length=100,
        min_length=4,
        required=True,
        label='Nombre (Requerido)',
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'required': 'true',
        }))

    nombre_empresa = forms.CharField(
        max_length=100,
        min_length=4,
        required=True,
        label='Nombre de la empresa (Requerido)',
        widget=forms.TextInput(attrs={
            'class': 'form-control',
        }))

    telefono = forms.CharField(
        max_length=14,
        min_length=8,
        required=True,
        label='Teléfono de contacto (Requerido)',
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'required': 'true',
        }))

    extension = forms.CharField(
        max_length=5,
        label='Extensión',
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
        }))

    telefono_adicional = forms.CharField(
        max_length=14,
        label='Teléfono adicional',
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
        }))
    cantidad_empleados = forms.IntegerField(
        min_value=1,
        label='Número de empleados',
        required=False,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )


class CodigoForm(forms.Form):
    codigo = forms.CharField(max_length=20,
                             label='Código de confirmación de la empresa',
                             widget=forms.TextInput(attrs={
                                 'class': 'form-control',
                             }))
    # captcha = ReCaptchaField(widget=ReCaptchaWidget())
