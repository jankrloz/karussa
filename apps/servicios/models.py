# -*- encoding: utf-8 -*-
import random
import string
from datetime import date

from django.db import models

from apps.catalogos.models import CatalogoDia
from apps.empresa.models import Empresa
from apps.usuarios.models import Usuario
from apps.videos.models import Video


class Plan(models.Model):
    titulo = models.CharField(max_length=100)
    jsonInfo = models.TextField()

    def __str__(self):
        return self.titulo

    def __unicode__(self):
        return self.titulo


'''
class VideosDia(models.Model):
    plan = models.ForeignKey(Plan)
    video = models.ForeignKey(Video)
    dia = models.ForeignKey(CatalogoDia)
    inicia = models.TimeField(blank=True, null=True)
    fin = models.TimeField(blank=True, null=True)
    fecha = models.DateField(null=True, blank=True)
    llave = models.CharField(default='', max_length=20)

    def save(self, *args, **kwargs):
        if self.llave == '' or not self.llave:
            self.llave = GenerarLLave()
        super(VideosDia, self).save(*args, **kwargs)

    def __str__(self):
        return self.plan.titulo + " " + self.video.titulo

    def __unicode__(self):
        return self.plan.titulo + " " + self.video.titulo

'''


class Paquete(models.Model):
    empresa = models.ForeignKey(Empresa)
    fecha_inicio = models.DateField(null=True, blank=True)
    fecha_fin = models.DateField(null=True, blank=True)
    pagado = models.BooleanField(default=False)
    activo = models.BooleanField(default=False)
    duracion = models.IntegerField(default=0)
    jsonInfo = models.TextField()
    monitores = models.IntegerField(default=0)
    godinez = models.IntegerField(default=0)

    def __str__(self):
        return self.empresa.nombre

    def __unicode__(self):
        return self.empresa.nombre

    def dame_numero_monitores(self):
        return self.plan.monitores


class VideosPaquete(models.Model):
    paquete = models.ForeignKey(Paquete)
    video = models.ForeignKey(Video)
    dia = models.DateField(default=date.today())
    inicia = models.TimeField(blank=True, null=True)
    fin = models.TimeField(blank=True, null=True)
    fecha = models.DateField(null=True, blank=True)
    llave = models.CharField(default='', max_length=20)

    def save(self, *args, **kwargs):
        if not self.llave:
            self.llave = GenerarLLave()
        super(VideosPaquete, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.video.titulo)

    def __unicode__(self):
        return str(self.video.titulo)


class Integrantes(models.Model):
    paquete = models.ForeignKey(Paquete)
    integrante = models.EmailField()
    monitor = models.EmailField()
    lista_nivel = (
        ("creador", "creador"),
        ("monitor", "monitor"),
        ("godinez", "godinez")
    )
    nivel = models.CharField(choices=lista_nivel, default='godinez', max_length=20)
    lista_estado = (
        ("Aceptado", "Aceptado"),
        ("Eliminado", "Eliminado"),
        ("Pendiente", "Pendiente")
    )
    estado = models.CharField(choices=lista_estado, max_length=30, default='Aceptado')

    def __str__(self):
        return str(self.monitor + " " + self.integrante)

    def __unicode__(self):
        return str(self.monitor + " " + self.integrante)


class ManagerFiltrarIntegrantesAdministrativos(models.Manager):
    def get_queryset(self):
        return super(ManagerFiltrarIntegrantesAdministrativos, self).get_queryset().filter(estado='Aceptado').exclude(
                nivel='godinez')


class IntegratesAdministrativos(Integrantes):
    class Meta:
        proxy = True

    object = ManagerFiltrarIntegrantesAdministrativos()


class ManagerFiltrarIntegrantesGodinez(models.Manager):
    def get_queryset(self):
        return super(ManagerFiltrarIntegrantesGodinez, self).get_queryset().filter(estado='Aceptado').filter(
                nivel='godinez')


class IntegrantesGodinez(Integrantes):
    class Meta:
        proxy = True

    object = ManagerFiltrarIntegrantesGodinez()


class Contacto(models.Model):
    nombre_contacto = models.CharField(max_length=100, verbose_name='Nombre de contacto', blank=True)
    nombre_empresa = models.CharField(max_length=100, verbose_name='Nombre de Empresa')
    telefono = models.CharField(max_length=20)
    extension = models.CharField(max_length=5)
    telefono_adicional = models.CharField(max_length=20, verbose_name='Teléfono adicional')
    email = models.CharField(max_length=50)
    # plan = models.ForeignKey(Plan)
    random = models.CharField(max_length=10)
    activo = models.BooleanField(default=False)
    fecha_creacion = models.DateTimeField(auto_now_add=True, blank=True)
    cantidad_empleados = models.IntegerField(default=1)

    # logo = models.Field()

    def __str__(self):
        return self.email


def GenerarLLave():
    codigo_prueba = genera_cadena_aleatorio(20)
    while existe_codigo(codigo_prueba):
        codigo_prueba = genera_cadena_aleatorio(20)
    return codigo_prueba


def genera_cadena_aleatorio(tamanio):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(tamanio))


def existe_codigo(codigo_parametro):
    return VideosPaquete.objects.filter(llave=codigo_parametro).exists()
