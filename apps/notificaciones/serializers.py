# -*- encoding: utf-8 -*-



from swampdragon.serializers.model_serializer import ModelSerializer

# ARCHIVO SERIALIZER
# especifica y 'convierte' el objeto Notificacion a un objeto entendible para las notificaciones en tiempo real del navegador


#Especificamos el modelo a serializar y los campos que retornara a la vista
class NotificacionSerializer(ModelSerializer):
    class Meta:
        # Modelo a serializar
        model = 'notificaciones.Notificacion'
        publish_fields = ['usuario','mensaje','url_video','imagen']

#Especificamos el modelo a serializar y los campos que retornara a la vista
class NotificacionVersion2Serializer(ModelSerializer):
    class Meta:
        # Modelo a serializar
        model = 'notificaciones.Notificacion_Version2'
        #publish_fields = ['usuario','mensaje','empresa','destinatario','id_opcional']
        publish_fields = ['usuario','mensaje','url_video','imagen']
        base_channel = 'karussa'




















