# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from apps.usuarios.models import Usuario
from apps.notificaciones.tasks import test, prueba, prueba_delay
import random

class Command(BaseCommand):
    help = 'lo sentimos pero se perdio el manual de ayuda'

    def handle(self, *args, **options):
        ##  Hara una tarea diez veces cada n tiempo
        i = 1
        n = random.randint(0,30)
        n = 30
        for i in range(i):
            print('Se ejecutara test.delay()')
            result = test.delay('CORRIENDO COMANDO _enviar_notifica.... ')
            print('metodos de funcion test')
            metodos = [method for method in dir(test) if callable(getattr(test, method))]
            print(metodos)

            print('type de result')
            print (type(result))
            print('resultado result.ready()')
            print(result.ready())
            print('vars de result')
            print(vars(result))
            print(result.ready())
            print('SCRAPED ')
            print('TIPO')
            print(type(scraper_example))
            print('METODOS ')

            metodos = [method for method in dir(scraper_example) if callable(getattr(scraper_example, method))]
            print(metodos)


            print('FUNCION QUE EN TEORIA SE EJECUTARA CADA CIERTO TIEMPO')
            scraper_example()
            print('Se debio de termina de ejecutar')
            print('FUNCION QUE EN TEORIA SE EJECUTARA CADA CIERTO TIEMPO aplicando DELAY')

            scraper_example.delay()
            print('Se debio de termina de ejecutar')

            print('CORRIENDO FUNCION CON UN DELAY ESPECIFICADO')
            prueba_delay.apply_async([n,] , countdown=n, expires=120)
            print('FIN FUNCION DELAY ESPECIFICADO')


        self.stdout.write('correo mensuales mandados')

