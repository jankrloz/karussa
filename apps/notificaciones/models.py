# -*- encoding: utf-8 -*-

from django.db import models
from swampdragon.models import SelfPublishModel
from apps.empresa.models import Empresa
#from apps.notificaciones.funciones import destinos
from .serializers import NotificacionSerializer, NotificacionVersion2Serializer
from apps.usuarios.models import Usuario


destinos = (
    ("global", "global"),
    ("godinez", "godinez"),
    ("monitores", "monitores"),
    ("usuario", "usuario"),
)


#   MODELO NOTIFICACIONES, PRIMER VERSION*
class Notificacion(SelfPublishModel, models.Model):
    serializer_class = NotificacionSerializer
    #usuario = models.ForeignKey(Usuario)
    mensaje = models.TextField()
    empresa = models.ForeignKey(Empresa)

    destinatario = models.CharField(choices=destinos, max_length=30, default='godinez')
    id_opcional = models.IntegerField(default=0)

#   MODELO NOTIFICACION PARA ACOPLARSE A LA APP
class Notificacion_Version2(SelfPublishModel, models.Model):
    serializer_class = NotificacionVersion2Serializer
    usuario = models.ForeignKey(Usuario)
    mensaje = models.TextField(default='')
    #empresa = models.ForeignKey(Empresa)
    url_video = models.TextField(default='')
    imagen = models.TextField(default='')
    creacion = models.DateTimeField(auto_now_add=True)
    entregado = models.BooleanField(default=False)

    destinatario = models.CharField(choices=destinos, max_length=30, default='godinez')
    id_opcional = models.IntegerField(default=0)


