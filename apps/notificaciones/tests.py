# -*- encoding: utf-8 -*-

from django.test import TestCase

# Create your tests here.
from swampdragon.testing.dragon_testcase import DragonTestCase

class FirstTest(DragonTestCase):
    def test_foo(self):
        self.connection.subscribe('notificaciones', 'bar', {})
        self.connection.call_verb('foo', 'do_foo', bar='test')
        message = self.connection.last_pub
        self.assertEqual(message['hello'], 'world')
