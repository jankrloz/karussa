# -*- encoding: utf-8 -*-
from __future__ import absolute_import

from celery import shared_task, task

# from celery.task.schedules import crontab
# from celery.decorators import periodic_task
from celery.utils.log import get_task_logger
from django.core.urlresolvers import reverse
from datetime import datetime
from apps.notificaciones.funciones_celery import listar_usuarios
from apps.usuarios.models import Usuario

logger = get_task_logger(__name__)


def suma(a, b):
    return (a + b)


'''
# A periodic task that will run every minute (the symbol "*" means every)
@periodic_task(run_every=(crontab(hour="*", minute="*", day_of_week="*")))
def scraper_example():
    print("Start task")

    #logger.info("Start task")
    now = datetime.now()
    result = suma(now.day, now.minute)
    #logger.info("Task finished: result = %i" % result)
    print("Task finished: result = %i" % result)

#print('METODOS ')
#metodos = [method for method in dir(scraper_example) if callable(getattr(scraper_example, method))]
#print(metodos)
'''


# apply_async(countdown=60, expires=120)
@task
def prueba_delay(param):
    logger.info('Usando task prueba delay con un tiempo de %s segundos' % param)


from apps.notificaciones.models import Notificacion_Version2


@task
def prueba_notificaciones_async(usuario_id, aumento):
    usuario = Usuario.objects.filter(id=usuario_id)[0]
    a = Notificacion_Version2.objects.create(usuario=usuario, mensaje=str(usuario.username)+' Mensaje desde comando tuneado para enviar notificaciones asyncs aumento=%s'%aumento,
                          url_video='TACO')
    logger.info('Notificacion creada :D' )
    print(a)
    return ("Devolviendo valor {0}".format(usuario_id))


@task
def crear_notificaciones_dia(usuario_id, llave):
    try:
        usuario = Usuario.objects.get(id=usuario_id)
        Notificacion_Version2.objects.create(usuario=usuario, mensaje="Hora de hacer Yoga!",
                                             url_video=reverse('videos_app:streaming_llave',
                                                               kwargs={'llave': llave}))
        return "se creo la notif..."
    except Exception as e:
        logger.exception("no se pudo encontrar al usuario o no se pudo crear la notificacion")
        logger.exception(e)


@task
def prueba(param):
    return ('Usando task %s ' % param)


# a = prueba.delay('taco')
# a.ready()
# a.result

@shared_task
def test(param):
    return ('The test task executed with argument "%s" ' % param).encode('utf-8')


# metodos = [method for method in dir(test) if callable(getattr(test, method))]
# print(metodos)



###########################################################################################################
#######     CADA DIEZ MINUTOS SE VAN A LISTAR USUARIOS, UN MINUTO DESPUES SE DEBE DE CREAR UN USUARIO NUEVO
##########################################################################################
# from celery.utils.log import get_task_logger
# logger = get_task_logger(__name__)

# @task
def testing_celery(mins=2):
    #   SE  MANDA A LLAMAR LA FUNCION listar_usuarios
    #   Aqui mismo mandamos a crear un usuario dos minutos posteriores a la peticion de listar usuarios
    segs = 60
    logger.info('INICIO TESTING_CELERY ')
    listar_usuarios(logger)
    n = segs * mins
    crear_usuario_random.apply_async(countdown=n, expires=120)
    # logger.info('termino de ejecutarse testing_celery ')
    return ('TERMINO DE EJECUTARSE  TESTING_CELERY ')


@task
def crear_usuario_random():
    #
    logger.info('INICIA CREAR_USUARIO_RANDOM')
    nombre_usuario = 'usuario_automatico'
    domain = '@domain.com'
    pswd = '123456'
    num_usuario = 1
    usuario = None
    while usuario == None:
        cad = '_00' + str(num_usuario)
        try:
            usuario = Usuario.objects.create(username=nombre_usuario, password=pswd, email=nombre_usuario + domain)
        except Exception as e:
            nombre_usuario += cad
            usuario = Usuario.objects.create(username=nombre_usuario, password=pswd, email=nombre_usuario + domain)
    # logger.info('SE CREO USUARIO CON USERNAME '+str(usuario.username))
    return ('SE CREO USUARIO CON USERNAME ' + str(usuario.username))
