# -*- encoding: utf-8 -*-

from django.db import models
from logging import Handler
from datetime import datetime
import traceback
from apps.usuarios.models import Usuario


class KarussaLog(Handler, object):
    def emit(self, record):
        try:
            log = Log()
            log.titulo = record.msg
            log.descripcion = ""
            log.nivel = record.levelname
            log.fecha = datetime.now()
            log.ubicacion = record.pathname + " - " + record.funcName + " - " + str(record.lineno)
            if hasattr(record, 'user_id'):
                log.usuario = int(record.user_id)
            else:
                log.usuario = 0
            if hasattr(record, 'exc_info') and record.exc_info is not None:
                etype, value, tb = record.exc_info
                tbformat = ''.join(traceback.format_exception(etype, value, tb))
                log.descripcion = tbformat
            else:
                log.descripcion = ""
            log.save()
        except Exception as e:
            pass


class Log(models.Model):
    usuario = models.IntegerField(default=0)
    titulo = models.TextField()
    nivel = models.CharField(max_length=100)
    descripcion = models.TextField()
    ubicacion = models.CharField(max_length=500)
    fecha = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        usuario = ''
        try:
            if self.usuario > 0:
                usuario = Usuario.objects.get(id=self.usuario)
                usuario = usuario.get_full_name
        except Exception as e:
            usuario = 'Anonimo'
        return str(self.titulo) + " " + usuario

    def __str__(self):
        usuario = ''
        try:
            if self.usuario > 0:
                usuario = Usuario.objects.get(id=self.usuario)
                usuario = usuario.get_full_name
        except Exception as e:
            usuario = 'Anonimo'
        return str(self.titulo) + " " + usuario

    def user(self):
        try:
            usuario = Usuario.objects.get(id=self.usuario)
            usuario = usuario.get_full_name
        except Exception as e:
            usuario = 'Anonimo'
        return usuario
