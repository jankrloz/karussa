# -*- encoding: utf-8 -*-

from logging import Filter
import logging
from django.conf import settings


class DebugLog(Filter, object):
    def filter(self, record):
        if record.levelname != "DEBUG":
            return True
        return settings.DEBUG


logger = logging.getLogger(settings.NOMBRE_LOG)
# Se maneja toda la logica del logger
logger.addFilter(DebugLog())
