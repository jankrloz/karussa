from karussa.settings.base import *
import pymysql

pymysql.install_as_MySQLdb()

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'test_karussa',
        'HOST': '162.243.186.32',
        'USER': 'dev',
        'PASSWORD': '=K:6qqnIWy967zw',
        'PORT': '3306',
    },
    'db_videos': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'test_videos',
        'HOST': '162.243.186.32',
        'USER': 'dev',
        'PASSWORD': 'Rd3nvK$',
        'PORT': '3306',
    }
}

'''
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db_karussa.sqlite3'),
    }
}
#'''

NOMBRE_LOG = "local"
DIRECCION_BASE = 'http://localhost:8000/'

DEBUG = True

#########   SETTINGS DE SWAMPDRAGON ##################

# Indispensable
#   Esta configuracion aplica si swampdragon no usa algun sistema de autenticacion o cosas asi
# SWAMP_DRAGON_CONNECTION = ('swampdragon.connections.sockjs_connection.DjangoSubscriberConnection', '/data')
# Esta configuracion ya trae implementado un modelo de usuario para swampdragon
SWAMP_DRAGON_CONNECTION = ('swampdragon_auth.socketconnection.HttpDataConnection', '/data')

# Va estar a la escucha de la ip publica
SWAMP_DRAGON_HOST = '0.0.0.0'
# puerto de escucha
SWAMP_DRAGON_PORT = '9999'  # default '9999'

# url en la que va estar escuchando para recibir mensajes y esas cosas
ip_ = '192.168.0.12'
ip_ = '127.0.0.1'
#DRAGON_URL = 'http://192.168.42.5:9999/' #PONER LA IP DE LA MAQUINA QUE ESTA CORRIENDO EL SERVIDOR REDIS y EL ARCHIVO remoto.py
#DRAGON_URL = 'http://192.168.43.238:9999/' #
DRAGON_URL = 'http://'+ip_+':9999/' #

#SWAMPDRAGON_TESTMODE = True


##********      TESTEANDO CELERY
# CELERY SETTINGS
#BROKER_URL = 'redis://'+ ip_ +':6379/0'
BROKER_URL = "amqp://guest:guest@"+ip_+":5672//"

CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'




