'''
from __future__ import absolute_import
import os
from celery import Celery
from django.conf import settings

# set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "karussa.settings.prod")
#os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'hello.settings')

app = Celery('karussa_django')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
'''
from __future__ import absolute_import
import os
from celery import Celery
from django.conf import settings

# set the default Django settings module for the 'celery' program.
#######3 cambiar para produccion o local, para encolar las acciones asyncronas
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "karussa.settings.prod")
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "karussa.settings.local")
from apps.logger.funciones import logger

app = Celery('Karussa_proyect :D ')
#app = Celery('Karussa_proyect :D ', backend="amqp", broker='amqp://guest@localhost:5672//')
#app = Celery('Karussa_proyect :D ', backend="amqp", broker='amqp://guest@localhost:5672//')

#app = Celery('Karussa_proyect :D ', backend]='redis://', broker='redis://')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
#logger.info('archivo celery.py ')


@app.task(bind=True)
def debug_task(self):
    logger.info('Request: {0!r}'.format(self.request))
