from django.conf import settings
from django.conf.urls import include, url, patterns
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'karussa.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^noSoy1admin/', include(admin.site.urls)),
    url(r'^', include('apps.empresa.urls', namespace='empresa_app')),
    url(r'^', include('apps.usuarios.urls', namespace='usuarios_app')),
    url(r'^superuser/', include('apps.superuser.urls', namespace='superuser_app')),
    url(r'^', include('apps.servicios.urls', namespace='servicios_app')),
    url(r'^', include('apps.notificaciones.urls', namespace='notificaciones_app')),
    url(r'^', include('apps.demo.urls', namespace='demo_app')),
    url(r'^', include('apps.videos.urls', namespace='videos_app')),
    url(r'^', include('apps.instaladores.urls', namespace='instaladores_app')),

    #url(r'^reset/password_reset/$', 'django.contrib.auth.views.password_reset',{'password_reset_form': EmailValidationOnForgotPassword , 'html_email_template_name':'mailing/mailing_renka_CAMBIA_TU_CONTRASENA.html', 'subject_template_name':'correos/asunto_password_reset.txt', 'from_email': 'RENKA <contacto@renka.com.mx>', }, name='reset_password_reset1'),
    #url(r'^reset/password_reset/done/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),
    #url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm',name='password_reset_confirm'),
    #url(r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete', name='password_reset_complete'),


]

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('', (
        r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}))
